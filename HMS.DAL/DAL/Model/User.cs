﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.DAL.DAL.Model
{
    public class User
    {
        public string UserName { get; set; }
        public int RoleId { get; set; }
    }
}
