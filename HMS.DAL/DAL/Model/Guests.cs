﻿using HMS.Model.Lookups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model
{
    public class Guests
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string IdProofType { get; set; }
        public string IdProofNumber { get; set; }
        public string CarModel { get; set; }
        public string RTGNumber { get; set; }
        public string Gender { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Phonenumber { get; set; }
        public string Emailid { get; set; }
        public string MoreIds { get; set; }
        public string Purposeofvisit { get; set; }
        public string Company { get; set; }
    }
}
