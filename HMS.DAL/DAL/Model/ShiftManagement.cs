﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HMS.DAL.DAL.Model
{
    public class ShiftManagement : INotifyPropertyChanged
    {
        private int _id;
        private string _fullName;
        private int _userId;
        private string _shiftName;
        private DateTime? _shiftStartDate;
        private DateTime? _shiftEndTime;
        private string _userName;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(); }
        }
        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; OnPropertyChanged(); }
            
        }
        public int UserId
        {
            get { return _userId; }
            set { _userId = value;  OnPropertyChanged(); }
        }
        public string ShiftName
        {
            get { return _shiftName; }
            set { _shiftName = value; OnPropertyChanged(); }
        }
        public DateTime? ShiftStartTime
        {
            get { return _shiftStartDate; }
            set { _shiftStartDate = value; OnPropertyChanged(); }
        }
        public DateTime? ShiftEndTime
        {
            get { return _shiftEndTime; }
            set { _shiftEndTime = value; OnPropertyChanged(); }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
