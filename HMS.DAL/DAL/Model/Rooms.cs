﻿using HMS.Model.Lookups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model
{
    public class Rooms
    {
      public int Id { get; set; }
      public int FloorId { get; set; }
      public string RoomNumber { get; set; }
      public string Room_Display { get; set; }
      public int StatusId { get; set; }
      public RoomType RoomType { get; set; }
    }
}
