﻿using HMS.Model.Lookups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model
{
    public class Arrivals
    {
        private Guests guest;

        public Arrivals(Guests guest, Rooms room)
        {
            Guest = guest;
            RoomId = room;
        }
        

        public Guests Guest { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime? CheckoutDate { get; set; }
        public int NumberofGuests { get; set; }
        public int NumberofDays { get; set; }
        public PaymentModes ModeofPaymentId { get; set; }
        
        public string GuestRemarks { get; set; }
        public Rooms RoomId { get; set; }
        public decimal? TaxExemption { get; set; }
        public decimal? RoomRate { get; set; }
        public decimal? RoomCharge { get; set; }
        public decimal? Cash { get; set; }
        public decimal? StateGST { get; set; }
        public decimal? CentralGST { get; set; }
        public decimal? Deposit { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Tax { get; set; }
        public int RoomType { get; set; }
        public decimal? Misc { get; set; }
        public string CardNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string OldRoom { get;  set; }
        public string TransferredRoom { get; internal set; }
        public int CVV { get; internal set; }
        public DateTime TentativeCheckoutDate { get; set; }
        public decimal? RoomServiceTax { get; set; }
        public string CheckinType { get; set; }
    }
}
