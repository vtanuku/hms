﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model
{
    public class AppMain
    {
       public int Id { get; set; }
       public string name { get; set; }
        public string DisplayName { get; set; }
        public string Address { get; set; }
    }
}
