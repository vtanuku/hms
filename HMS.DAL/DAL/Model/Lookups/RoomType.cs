﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Model.Lookups
{
    public class RoomType
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public decimal RoomCharge { get; set; }
        public decimal CentralGST { get; set; }
        public decimal StateGST { get; set; }
        public decimal? RoomServiceTax { get; set; }
    }
}
