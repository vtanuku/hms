﻿using HMS.Model.Lookups;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.DAL.Reposotories
{
    public static class LookupRepository
    {
        private static IList<Status> _statusList;
        static string Query_GetAllStatus = "select * from LookupStatus";

        public static IList<Status> GetAllRoomStatus()
        {
            if(_statusList != null)
            {
                return _statusList;
            }
            else
            {
                _statusList = new List<Status>();
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["HMS"].ConnectionString);                
                con.Open();
                SqlCommand cmd = new SqlCommand(Query_GetAllStatus, con);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Status modelObject = new Status();
                    modelObject.Id = Convert.ToInt32(dr["id"]);
                    modelObject.Code = dr["name"].ToString();
                    modelObject.Description = dr["description"].ToString();
                    _statusList.Add(modelObject);
                }
                return _statusList;
            }
        }

    }
}
