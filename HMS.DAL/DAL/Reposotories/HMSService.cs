﻿using HMS.Model;
using HMS.Core.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMS.Model.Lookups;
using System.Data;
using static HMS.Core.Enums.ReportEnums;
using System.Runtime.InteropServices;
using HMS.Core.Enums;
using System.Data.SqlTypes;
using HMS.DAL.DAL.Model;
using System.Windows;
using System.Globalization;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web.UI;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;

namespace HMS.DAL.Reposotories
{
    public class HMSService
    {
        string sql = "select username, password from users (nolock) where username = @username";
        string hotelNameSQL = "select name, displayname, Address from AppMain (nolock)";
        string getRooms = "select id, floor_id, room_no, room_display, status, room_type from rooms (nolock)";
        string getFloors = "select id,floor_name from floors (nolock)";
        string getGuests = "select Id,FullName,DateOfBirth,Address,IdProofId,IdProofNumber,CarModel,RTGNum,gender,zipcode,city,state,phonenumber,emailid,MoreIds,purposeofvisit,company from Guests (nolock) where id = @guestid";
        string getRoomTypes = "SELECT id,description,code,RoomCharge,CentralGST,StateGST,ServiceTax from LookupRoomType";

        string getModesofPayment = "select id, code, description from ModesofPayment";
        string getStatuses = "select id, name, description from LookupStatus";
        string getProofs = "select id, code, description from LookupProofs";
        string getArrivals = "SELECT top 1 GuestId,CheckInDate,CheckoutDate,NumberofGuests,NumberofDays,ModeofPaymentId,GuestRemarks,RoomId,taxExemption,RoomCharge,RoomRate,oldroom,transferredroom,expiredate,CVV,deposit,CGST,SGST,cardnumber,misc,balance,TentativeCheckout, ServiceTax,CheckinType FROM Arrivals where roomid = @RoomId and CheckoutDate is null ORDER BY creation_date desc";

        string dailyRoomPlusTax = @"select rooms.room_no as RoomNumber, SUM(Arrivals.taxExemption) as TaxExemption, SUM(Arrivals.CGST) as CentralGST, SUM(Arrivals.SGST) as StateGST,   (SUM(CGST) + SUM(SGST) - SUM(taxExemption)) as RoomTax from rooms 
                                    INNER join 
                                    Arrivals on 
                                    rooms.id = Arrivals.RoomId
                                    where creation_date >= @startdate and creation_date <= @enddate
                                    GROUP by rooms.room_no";

        string dailyAuditPlusTax = @"select rooms.room_no as RoomNumber, SUM(Arrivals.RoomCharge) as RoomCharge, SUM(Arrivals.CGST) as CentralGST, SUM(Arrivals.SGST) as StateGST,SUM(Arrivals.taxExemption) as TaxExemption, SUM(Arrivals.deposit) as Deposits, SUM(Arrivals.misc) as MISC, (SUM(RoomCharge) + SUM(CGST) + SUM(SGST) - SUM(taxExemption)) as RoomRevenue from rooms 
                                    INNER join 
                                    Arrivals on 
                                    rooms.id = Arrivals.RoomId
                                    where creation_date >= @startdate and creation_date <= @enddate
                                    GROUP by rooms.room_no";
        string updateArrivalDetails = @"UPDATE Arrivals set CheckInDate = @CheckInDate, CheckoutDate = @CheckoutDate, NumberofGuests = @NumberofGuests, NumberofDays = @NumberofDays, taxExemption = @taxExemption, RoomCharge = @RoomCharge, RoomRate = @RoomRate, oldroom = @oldroom, deposit = @deposit, CGST = @CGST, SGST = @SGST, misc = @misc, balance = @balance, TentativeCheckout = @TentativeCheckout, ServiceTax = @ServiceTax, CheckinType = @CheckinType
                                        where GuestId = @GuestId and RoomId = @RoomId";

        string incomebyDateQuery = @"select Arrivals.creation_date as Date, SUM(Arrivals.RoomCharge) as RoomCharge, SUM(Arrivals.CGST) as CentralGST, SUM(Arrivals.SGST) as StateGST,SUM(Arrivals.taxExemption) as TaxExemption, SUM(Arrivals.deposit) as Deposits, SUM(Arrivals.misc) as MISC, (SUM(RoomCharge) + SUM(CGST) + SUM(SGST) - SUM(taxExemption) - SUM(Misc)) as RoomRevenue from rooms 
                                    INNER join 
                                    Arrivals on 
                                    rooms.id = Arrivals.RoomId
                                    where creation_date >= @startdate and creation_date <= @enddate
                                    GROUP by Arrivals.creation_date
                                    order by Arrivals.creation_date ";

        string incomebyMonthQuery = @"select datename(month, Arrivals.creation_date) as Date, SUM(Arrivals.RoomCharge) as RoomCharge, SUM(Arrivals.CGST) as CentralGST, SUM(Arrivals.SGST) as StateGST,SUM(Arrivals.taxExemption) as TaxExemption, SUM(Arrivals.deposit) as Deposits, SUM(Arrivals.misc) as MISC, (SUM(RoomCharge) + SUM(CGST) + SUM(SGST) - SUM(taxExemption) - SUM(Misc)) as RoomRevenue from rooms 
                                    INNER join 
                                    Arrivals on 
                                    rooms.id = Arrivals.RoomId
                                    where creation_date >= @startdate and creation_date <= @enddate
                                    GROUP by datename(month, Arrivals.creation_date)
                                    order by datename(month, Arrivals.creation_date)";

        string roomRevenueByDateRange = @"select Arrivals.creation_date, SUM(Arrivals.RoomCharge) as RoomCharge, SUM(Arrivals.CGST) as CentralGST, SUM(Arrivals.SGST) as StateGST,SUM(Arrivals.taxExemption) as TaxExemption, SUM(Arrivals.deposit) as Deposits, SUM(Arrivals.misc) as MISC, (SUM(RoomCharge) + SUM(CGST) + SUM(SGST) - SUM(taxExemption) - SUM(Misc)) as RoomRevenue from rooms 
                                    INNER join 
                                    Arrivals on 
                                    rooms.id = Arrivals.RoomId
                                    where creation_date >= @startdate and creation_date <= @enddate
									and rooms.room_no = @RoomNumber
                                    GROUP by Arrivals.creation_date";

        string taxExemptionsTransaction = "select CAST(CheckInDate AS DATE) as DateOfReport , SUM((RoomCharge * taxExemption)/100) as roomtaxexemption  from Arrivals where CheckInDate>=@startdate and CheckInDate <= @enddate  GROUP BY CAST(CheckInDate AS DATE)";

        string checkinGuest = @"insert into Guests
                                    (FullName,DateOfBirth,Address,IdProofId,IdProofNumber,CarModel,RTGNum,gender,zipcode,city,state,phonenumber,MoreIds,purposeofvisit,company)
                                    VALUES
                                    (@fullName,@dateofbirth,@address,@idproofid,@idproofnumber,@carmodel,@RTGNum,@gender,@zipcode,@city,@state,@phonenumber,@MoreIds,@purposeofvisit,@company)";

        string updateRoomType = @"update LookupRoomType Set description=@description,RoomCharge=@RoomCharge,CentralGST=@CentralGST,StateGST=@StateGST, ServiceTax=@ServiceTax where Id=@Id";
        string insertRoomType = @"insert into LookupRoomType
                                    (id,description,code,RoomCharge,CentralGST,StateGST, ServiceTax)
                                    VALUES
                                    ((select max(id)+1 from LookupRoomType),@description,@code,@RoomCharge,@CentralGST,@StateGST, @ServiceTax)";
        string updateRoomDetails = @"update rooms set floor_id = @floorid,room_no=@roomno,room_display=@roomdisplay,ROOM_TYPE=@roomtype where id = @id";
        string insertRoomDetails = @"insert into rooms
                                    (floor_id,room_no,room_display,ROOM_TYPE, status)
                                    VALUES
                                    (@floorid,@roomno,@roomdisplay,@roomtype, 2)";
        string extendCheckinTime = @"update Arrivals
                                        set CheckoutDate = @CheckoutDate,
	                                        NumberofDays = @NumberofDays,
	                                        TentativeCheckout = @TentativeCheckout
                                        where GuestId = @Guestid and RoomId = @RoomId";
        string insertArrival = @"insert into Arrivals
                                    (GuestId,CheckInDate,TentativeCheckout,NumberofGuests,NumberofDays,ModeofPaymentId,GuestRemarks,RoomId,taxExemption,RoomCharge,RoomRate,oldroom,transferredroom,expiredate,CVV,deposit,CGST,SGST,cardnumber,misc,balance,creation_date,CheckinType)
                                    VALUES
                                    (@GuestId,@CheckInDate,@TentativeCheckout,@NumberofGuests,@NumberofDays,@ModeofPaymentId,@GuestRemarks,@RoomId,@taxExemption,@RoomCharge,@RoomRate,@oldroom,@transferredroom,@expiredate,@CVV,@deposit,@CGST,@SGST,@cardnumber,@misc,@balance,@creation_date,@CheckinType)";

        string checkoutGuest = @"Update Arrivals Set CheckoutDate = @CheckoutDate where GuestId=@GuestId and RoomId=@RoomId";

        string insertUser = @"insert into users
                                (username,password,fullname,RoleId)
                                VALUES
                                (@username,@password,@fullname,@roleid)";

        string markRoomAsDirty = @"Update Rooms set status = 4 where Id = @RoomId";

        string markRoomForMaintenance = @"Update Rooms set status = 3 where Id = @RoomId";

        string markRoomAsReady = @"Update Rooms set status = 2 where Id = @RoomId";

        string markRoomAsOccupied = @"Update Rooms set status = 1 where Id = @RoomId";
        string updateStatusofRoom = @"Update rooms set status = @StatusId where Id= @RoomId";
        string InsertAUser = @"insert into users
                                (username,password, fullname,RoleId)
                                values
                                (@username,@password,@fullname,@RoleId)";
        string StartAShift = @"insert into shifts
                                (userid,shiftname,logindate,logoutdate)
                                values
                                (@userid,@shiftname,@logindate,@logoutdate)";

        string LogoutShift = @"update shifts set logoutdate = @logoutdate where  shiftname = @shiftname and userid=@userid";

        string GetUsers = @"select username, RoleId from users with (nolock)";
        string GetShifts = @"select u.id, u.fullname, u.username, s.ShiftName, s.ShiftStartTime, s.ShiftEndTime from users u left outer join shiftmanagements s on u.id = s.userid where u.RoleId = 2";
        string updateShiftsManagement = @"update shiftmanagements set ShiftName = @ShiftName, ShiftStartTime = @ShiftStartTime,  ShiftEndTime =  @ShiftEndTime where userid = @userid";
        string insertShiftManagement = @"insert into shiftmanagements
                                         (UserId,ShiftName,ShiftStartTime,ShiftEndTime)
                                         VALUES
                                         (@UserId,@ShiftName,@ShiftStartTime,@ShiftEndTime)";
        string getShiftOfUser = "select * from shiftmanagements where userid =@userid";

        private string reportGenerationPath = ConfigurationManager.AppSettings["ReportGenerationFolder"] + "\\";
        public void CheckOutGuest(Guests _guest, Arrivals _arrival)
        {
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(checkoutGuest, conn))
                {
                    cmd.Parameters.Add(new SqlParameter("@GuestId", SqlDbType.Int) { Value = _guest.Id });
                    cmd.Parameters.Add(new SqlParameter("@RoomId", SqlDbType.Int) { Value = _arrival.RoomId?.Id });
                    cmd.Parameters.Add(new SqlParameter("@CheckoutDate", SqlDbType.DateTime) { Value = DateTime.Now });
                    cmd.ExecuteNonQuery();
                }
                ShiftToMaintenance(_arrival.RoomId);
                MessageBox.Show("Guest Checkedout Successfully and room switched to maintenance");
            }
        }

        public void ShiftToMaintenance(Rooms roomId)
        {
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(markRoomForMaintenance, conn))
                {
                    cmd.Parameters.Add(new SqlParameter("@RoomId", roomId.Id));
                    cmd.ExecuteNonQuery();
                }
            }
            MessageBox.Show("Successful");
        }

        public List<ShiftManagement> GetShiftManagements()
        {
            List<ShiftManagement> _lists = new List<ShiftManagement>();
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(GetShifts, conn))
                {
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        var shiftMgmt = new ShiftManagement();
                        shiftMgmt.UserId = dr.GetInt32(0);
                        shiftMgmt.FullName = dr.IsDBNull(1) ? string.Empty : dr.GetString(1);
                        shiftMgmt.UserName = dr.IsDBNull(2) ? string.Empty : dr.GetString(2);
                        shiftMgmt.ShiftName = dr.IsDBNull(3) ? string.Empty : dr.GetString(3);
                        shiftMgmt.ShiftStartTime = dr.IsDBNull(4) ? (DateTime?)null : dr.GetDateTime(4);
                        shiftMgmt.ShiftEndTime = dr.IsDBNull(5) ? (DateTime?)null : dr.GetDateTime(5);
                        _lists.Add(shiftMgmt);
                    }
                }
            }
            return _lists;
        }

        public List<User> GetUsersList()
        {
            List<User> _lists = new List<User>();
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(GetUsers, conn))
                {
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        var user = new User();
                        user.UserName = dr.GetString(0);
                        user.RoleId = dr.IsDBNull(1) ? 0 : dr.GetInt32(1);
                        _lists.Add(user);
                    }
                }
            }
            return _lists;
        }

        public void ShiftToNormal(Rooms room)
        {
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(markRoomAsReady, conn))
                {
                    cmd.Parameters.Add(new SqlParameter("@RoomId", room.Id));
                    cmd.ExecuteNonQuery();
                }
            }

            MessageBox.Show("Room is now ready for checkin");
        }

        public void UpdateAllUserShifts(int UserId, string ShiftName, DateTime? ShiftStartTime, DateTime? ShiftEndTime)
        {
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(getShiftOfUser, conn))
                {
                    cmd.Parameters.Add(new SqlParameter("@userid", UserId));
                    if (cmd.ExecuteScalar() == null)
                    {
                        InsertShiftManagement(UserId, ShiftName, ShiftStartTime, ShiftEndTime);
                    }
                    else
                    {
                        UpdateShifts(UserId, ShiftName, ShiftStartTime, ShiftEndTime);
                    }
                }
            }
            MessageBox.Show("Successful");
        }

        public void InsertShiftManagement(int UserId, string ShiftName, DateTime? ShiftStartTime, DateTime? ShiftEndTime)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertShiftManagement, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@UserId", UserId));
                        cmd.Parameters.Add(new SqlParameter("@ShiftName", ShiftName));
                        cmd.Parameters.Add(new SqlParameter("@ShiftStartTime", ShiftStartTime ?? SqlDateTime.Null));
                        cmd.Parameters.Add(new SqlParameter("@ShiftEndTime", ShiftEndTime ?? SqlDateTime.Null));
                        cmd.ExecuteNonQuery();
                    }
                }
                MessageBox.Show("Successful");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error updating Shift Management" + ex.Message);
            }
        }

        public void UpdateShifts(int UserId, string ShiftName, DateTime? ShiftStartTime, DateTime? ShiftEndTime)
        {
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(updateShiftsManagement, conn))
                {
                    cmd.Parameters.Add(new SqlParameter("@UserId", UserId));
                    cmd.Parameters.Add(new SqlParameter("@ShiftName", ShiftName));
                    cmd.Parameters.Add(new SqlParameter("@ShiftStartTime", ShiftStartTime));
                    cmd.Parameters.Add(new SqlParameter("@ShiftEndTime", ShiftEndTime));
                    cmd.ExecuteNonQuery();
                }
            }
            MessageBox.Show("Successful");
        }


        public void CheckInGuest(Guests _guest, Arrivals _arrival, bool SetCheckout = false)
        {
            try
            {
                _arrival.RoomId = GetRooms().FirstOrDefault(c => c.RoomNumber == _arrival.RoomId?.RoomNumber);
                using (SqlConnection connection =
                new SqlConnection(sqlConnectionString))
                {

                    using (SqlCommand command = new SqlCommand(checkinGuest, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@fullName", SqlDbType.VarChar) { Value = _guest.FullName ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@dateofbirth", SqlDbType.DateTime) { Value = _guest.DateOfBirth == DateTime.MinValue ? SqlDateTime.Null : _guest.DateOfBirth });
                        command.Parameters.Add(new SqlParameter("@address", SqlDbType.NVarChar) { Value = _guest.Address ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@idproofid", SqlDbType.NVarChar) { Value = _guest.IdProofType ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@idproofnumber", SqlDbType.NVarChar) { Value = _guest.IdProofNumber ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@carmodel", SqlDbType.NVarChar) { Value = _guest.CarModel ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@RTGNum", SqlDbType.NVarChar) { Value = _guest.RTGNumber ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@gender", SqlDbType.NVarChar) { Value = _guest.Gender ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@zipcode", SqlDbType.NVarChar) { Value = _guest.Zipcode ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@city", SqlDbType.NVarChar) { Value = _guest.City ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@state", SqlDbType.VarChar) { Value = _guest.State ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@phonenumber", SqlDbType.VarChar) { Value = _guest.Phonenumber ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@MoreIds", SqlDbType.NVarChar) { Value = _guest.MoreIds ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@purposeofvisit", SqlDbType.NVarChar) { Value = _guest.Purposeofvisit ?? string.Empty });
                        command.Parameters.Add(new SqlParameter("@company", SqlDbType.NVarChar) { Value = _guest.Company ?? string.Empty });

                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();
                    }

                    using (SqlCommand command = new SqlCommand("select max(id) from Guests", connection))
                    {
                        connection.Open();
                        _guest.Id = (int)command.ExecuteScalar();
                        connection.Close();
                    }
                    // Create the Command and Parameter objects.
                    using (SqlCommand command = new SqlCommand(insertArrival, connection))
                    {
                        // Open the connection in a try/catch block. 
                        // Create and execute the DataReader, writing the result
                        // set to the console window.
                        command.Parameters.Add(new SqlParameter("@GuestId", _guest.Id));
                        command.Parameters.Add(new SqlParameter("@CheckInDate", _arrival.CheckInDate == DateTime.MinValue ? DateTime.MinValue : _arrival.CheckInDate));
                        command.Parameters.Add(new SqlParameter("@TentativeCheckout", _arrival.TentativeCheckoutDate == DateTime.MinValue ? SqlDateTime.Null : _arrival.TentativeCheckoutDate));
                        command.Parameters.Add(new SqlParameter("@NumberofGuests", _arrival.NumberofGuests == null ? 0 : _arrival.NumberofGuests));
                        command.Parameters.Add(new SqlParameter("@NumberofDays", _arrival.NumberofDays == null ? 0 : _arrival.NumberofDays));
                        command.Parameters.Add(new SqlParameter("@ModeofPaymentId", _arrival.ModeofPaymentId?.Id ?? 0));
                        command.Parameters.Add(new SqlParameter("@GuestRemarks", _arrival.GuestRemarks ?? string.Empty));
                        command.Parameters.Add(new SqlParameter("@RoomId", _arrival.RoomId?.Id));
                        command.Parameters.Add(new SqlParameter("@taxExemption", _arrival.TaxExemption ?? 0));
                        command.Parameters.Add(new SqlParameter("@RoomCharge", _arrival.RoomCharge ?? 0));
                        command.Parameters.Add(new SqlParameter("@RoomRate", _arrival.RoomRate ?? 0));
                        command.Parameters.Add(new SqlParameter("@oldroom", _arrival.OldRoom ?? string.Empty));
                        command.Parameters.Add(new SqlParameter("@transferredroom", _arrival.TransferredRoom ?? string.Empty));
                        command.Parameters.Add(new SqlParameter("@expiredate", _arrival.ExpiryDate == DateTime.MinValue ? SqlDateTime.Null : _arrival.ExpiryDate));
                        command.Parameters.Add(new SqlParameter("@CVV", _arrival.CVV == null ? 0 : _arrival.CVV));
                        command.Parameters.Add(new SqlParameter("@deposit", _arrival.Deposit ?? 0));
                        command.Parameters.Add(new SqlParameter("@CGST", _arrival.CentralGST ?? 0));
                        command.Parameters.Add(new SqlParameter("@SGST", _arrival.StateGST ?? 0));
                        command.Parameters.Add(new SqlParameter("@cardnumber", string.IsNullOrWhiteSpace(_arrival.CardNumber) ? "" : _arrival.CardNumber));
                        command.Parameters.Add(new SqlParameter("@misc", _arrival.Misc ?? 0));
                        command.Parameters.Add(new SqlParameter("@balance", _arrival.Balance ?? 0));
                        command.Parameters.Add(new SqlParameter("@creation_date", DateTime.Now));
                        command.Parameters.Add(new SqlParameter("@CheckinType", _arrival.CheckinType ?? string.Empty));
                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                UpdateStatusOfRoom(_arrival.RoomId.Id, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Checking in the guest into room + " + _arrival.RoomId?.Id + "." + Environment.NewLine + "Please verify if you have entered all the details in the form."
                    + Environment.NewLine + Environment.NewLine);
            }
        }

        public void ShiftToDirty(Rooms room)
        {
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(markRoomAsDirty, conn))
                {
                    cmd.Parameters.Add(new SqlParameter("@RoomId", room.Id));
                    cmd.ExecuteNonQuery();
                }
            }
            MessageBox.Show("Room marked as Dirty");
        }

        public void TransferRoom(Guests _guest, Arrivals _arrival)
        {

        }


        public void GetDalilyReportsForAudit(DateTime selectedStartDate, DateTime selectedEndDate, EnumReportType dailyAuditReport)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application oXL;
                Microsoft.Office.Interop.Excel._Workbook oWB;
                Microsoft.Office.Interop.Excel._Worksheet oSheet;
                Microsoft.Office.Interop.Excel.Range oRng;
                object misvalue = System.Reflection.Missing.Value;
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;
                int rowCount = 5;
                decimal roomCharge = 0;
                decimal roomCGST = 0;
                decimal roomSGST = 0;
                decimal roomTaxExemption = 0;
                decimal roomDeposits = 0;
                decimal roomMisc = 0;
                decimal roomRevenue = 0;
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(dailyAuditPlusTax, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@startdate", SqlDbType.DateTime));
                        cmd.Parameters.Add(new SqlParameter("@enddate", SqlDbType.DateTime));
                        cmd.Parameters["@startdate"].Value = selectedStartDate;
                        cmd.Parameters["@enddate"].Value = selectedEndDate;
                        SqlDataReader dr = cmd.ExecuteReader();
                        oSheet.Cells[1, 1] = _hotel.name;
                        oSheet.Cells[1, 4] = dailyAuditReport.ToString();
                        oSheet.Cells[3, 3] = "DateRange from " + selectedStartDate.ToShortDateString() + " to "
                                            + selectedEndDate.ToShortDateString();
                        // Loop through the fields and add headers
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            string name = dr.GetName(i);
                            if (name.Contains(","))
                                name = "\"" + name + "\"";
                            oSheet.Cells[4, i + 1] = name;
                        }
                        //oSheet.Cells[4, dr.FieldCount + 4] = "Room Tax";
                        oSheet.get_Range("A" + 1, "H" + 4).Font.Bold = true;
                        oSheet.get_Range("A1", "E8").VerticalAlignment =
                   Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                        // Loop through the rows and output the data
                        while (dr.Read())
                        {

                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string value = dr[i].ToString();
                                if (value.Contains(","))
                                    value = "\"" + value + "\"";
                                oSheet.Cells[rowCount, i + 1] = value;
                                if (i == 1)
                                    roomCharge = roomCharge + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                if (i == 2)
                                    roomCGST = roomCGST + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                if (i == 3)
                                    roomSGST = roomSGST + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                if (i == 4)
                                    roomTaxExemption = roomTaxExemption + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                if (i == 5)
                                    roomDeposits = roomDeposits + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                if (i == 6)
                                    roomMisc = roomMisc + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                if (i == 7)
                                    roomRevenue = roomRevenue + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                //fs.Write(value + ",");
                            }

                            rowCount++;
                            //oSheet.Cells[rowCount, dr.FieldCount] = roomTax;
                            //fs.WriteLine();
                        }
                        //fs.Close();
                    }

                    oSheet.Cells[rowCount + 2, 2] = "Total Room Charge";
                    oSheet.get_Range("B" + rowCount + 7).Font.Bold = true;

                    oSheet.Cells[rowCount + 2, 3] = roomCharge;


                    oSheet.Cells[rowCount + 3, 2] = "Total Central GST";
                    oSheet.Cells[rowCount + 3, 3] = roomCGST;

                    oSheet.Cells[rowCount + 4, 2] = "Total State GST";
                    oSheet.Cells[rowCount + 4, 3] = roomSGST;

                    oSheet.Cells[rowCount + 4, 2] = "Total State GST";
                    oSheet.Cells[rowCount + 4, 3] = roomSGST;

                    oSheet.Cells[rowCount + 4, 2] = "Total State GST";
                    oSheet.Cells[rowCount + 4, 3] = roomSGST;

                    oSheet.Cells[rowCount + 5, 2] = "TOTAL TAX EXEMPTION";
                    oSheet.Cells[rowCount + 5, 3] = roomTaxExemption;

                    oSheet.Cells[rowCount + 6, 2] = "TOTAL MISC";
                    oSheet.Cells[rowCount + 6, 3] = roomMisc;

                    oSheet.Cells[rowCount + 3, 7] = "TOTAL Revenue";
                    oSheet.Cells[rowCount + 3, 8] = roomRevenue - roomMisc;

                    oSheet.get_Range("A1", "H100").Columns.AutoFit();
                    oXL.Visible = false;
                    oXL.UserControl = false;

                    oWB.SaveAs(reportGenerationPath + "DailyAuditReport" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                        false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(oSheet);
                    Marshal.ReleaseComObject(oWB);
                    MessageBox.Show("Report Create at:" + reportGenerationPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Generating Report." + Environment.NewLine + ex.Message);
            }
        }

        string monthlyReports = "select MONTH(CheckInDate) as month, Sum(RoomTotal) from Arrivals where CheckInDate >= @startdate and CheckInDate <= @enddate group by MONTH(CheckInDate)";

        public void GetDateRangeIncomeDateRange(DateTime selectedStartDate, DateTime selectedEndDate, EnumReportType reportType)
        {
            System.Data.DataTable dataTable = new System.Data.DataTable();
            try
            {

                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(incomebyDateQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@startdate", SqlDbType.DateTime));
                        cmd.Parameters.Add(new SqlParameter("@enddate", SqlDbType.DateTime));
                        cmd.Parameters["@startdate"].Value = selectedStartDate;
                        cmd.Parameters["@enddate"].Value = selectedEndDate;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        // this will query your database and return the result to your datatable
                        da.Fill(dataTable);
                        da.Dispose();
                    }
                }
                string html = string.Empty;
                html += "<font size=\"2\" face=\"Calibri\">";
                
                html += "<table><tr>";
                html += "<td>";
                html += "<strong>";
                html += _hotel.name;
                html += "</strong>";
                html += "</td>";
                html += "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
                html += "<td><strong>Income By Date Range</strong></td>";
                html += "</tr></table>";
                html += "<table><tr>";
                html += "<td>";
                html += "<strong>";
                html += "DATE RANGE FROM " + selectedStartDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) + " TO " + selectedEndDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
                html += "</strong>";
                html += "</td>";
                html += "</tr></table>";
                
                html += "</font>";
                html = ConvertDataTableToHTML(dataTable, html);
                

                Document document = new Document();
                PdfWriter.GetInstance(document, new FileStream(Environment.CurrentDirectory + "\\MySamplePDF.pdf", FileMode.Create));
                document.Open();
                iTextSharp.text.html.simpleparser.HTMLWorker hw =
                new iTextSharp.text.html.simpleparser.HTMLWorker(document);
                hw.Parse(new StringReader(html));
                document.Close();
                System.IO.File.WriteAllText(@"C:\yoursite.htm", html);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error generating DateRange Income:" + Environment.NewLine + ex.Message);
            }
        }

        public static string ConvertDataTableToHTML(System.Data.DataTable dt, string html)
        {
            html += "<font size=\"2\" face=\"Calibri\">";
            
            html += "<table border=\"1\">";
            //add header row
            html += "<tr>";
            for (int i = 0; i < dt.Columns.Count; i++)
                html += "<th>" + dt.Columns[i].ColumnName + "</th>";
            html += "</tr>";

            //add rows
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                {   if (dt.Rows[i][j].GetType() == typeof(DateTime))
                    {
                      html += "<td align=\"Center\">" + Convert.ToDateTime(dt.Rows[i][j]).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) + "</td>"; 
                    }
                    else
                    {
                        html += "<td align=\"Center\">" + dt.Rows[i][j].ToString() + "</td>";
                    }
                }
                html += "</tr>";
            }
            html += "</table>";
            html += "</font>";
            

            return html;
        }

        //public void GetDateRangeIncomeDateRange(DateTime selectedStartDate, DateTime selectedEndDate, EnumReportType reportType)
        //{
        //    var table = new HtmlTable();
        //    string html = string.Empty;

        //    using (SqlConnection conn = new SqlConnection(sqlConnectionString))
        //    {
        //        conn.Open();
        //        using (SqlCommand cmd = new SqlCommand(incomebyDateQuery, conn))
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@startdate", SqlDbType.DateTime));
        //            cmd.Parameters.Add(new SqlParameter("@enddate", SqlDbType.DateTime));
        //            cmd.Parameters["@startdate"].Value = selectedStartDate;
        //            cmd.Parameters["@enddate"].Value = selectedEndDate;
        //            SqlDataReader dr = cmd.ExecuteReader();
        //            oSheet.Cells[1, 1] = _hotel.name;
        //            oSheet.Cells[1, 4] = reportType.ToString();
        //            oSheet.Range[oSheet.Cells[1, 4], oSheet.Cells[1, 6]].Merge();
        //            oSheet.Cells[3, 3] = "DateRange from " + selectedStartDate.ToShortDateString() + " to "
        //                                + selectedEndDate.ToShortDateString();
        //            oSheet.Range[oSheet.Cells[3, 3], oSheet.Cells[3, 6]].Merge();

        //            // Loop through the fields and add headers
        //            for (int i = 0; i < dr.FieldCount; i++)
        //            {
        //                string name = dr.GetName(i);
        //                if (name.Contains(","))
        //                    name = "\"" + name + "\"";
        //                oSheet.Cells[4, i + 1] = name;
        //            }

        //            //oSheet.Cells[4, dr.FieldCount + 4] = "Room Tax";

        //            oSheet.get_Range("A" + 1, "H" + 4).Font.Bold = true;
        //            oSheet.get_Range("A1", "H4").VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

        //            // Loop through the rows and output the data
        //            while (dr.Read())
        //            {

        //                for (int i = 0; i < dr.FieldCount; i++)
        //                {
        //                    string value = string.Empty;
        //                    if (i == 0)
        //                    {
        //                        value = dr.GetDateTime(0).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
        //                    }
        //                    else
        //                        value = dr[i].ToString();
        //                    if (value.Contains(","))
        //                        value = "\"" + value + "\"";
        //                    oSheet.Cells[rowCount, i + 1] = value;
        //                    //fs.Write(value + ",");
        //                }
        //                rowCount++;
        //                //oSheet.Cells[rowCount, dr.FieldCount] = roomTax;
        //                //fs.WriteLine();
        //            }
        //            //fs.Close();
        //        }

        //        oSheet.get_Range("A1", "A100").Columns.AutoFit();
        //        oSheet.get_Range("A1", "H100").WrapText = true;
        //        oSheet.get_Range("A1", "H100").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
        //        oXL.Visible = false;
        //        oXL.UserControl = false;
        //        oWB.SaveAs(reportGenerationPath + "IncomeByDateRange" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
        //            false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
        //            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //        //oWB.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, reportGenerationPath + "IncomeByDateRange" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls");

        //        Marshal.FinalReleaseComObject(oSheet);
        //        Marshal.FinalReleaseComObject(oWB);
        //        Marshal.FinalReleaseComObject(oXL);
        //        MessageBox.Show("Report Create at:" + reportGenerationPath);
        //    }
        //    //    HtmlTableRow row;
        //    //    HtmlTableCell cell;


        //    //        row = new HtmlTableRow();
        //    //        cell = new HtmlTableCell();
        //    //        cell.InnerText = invalidCompany.BusinessName;
        //    //        row.Cells.Add(cell);
        //    //        cell.InnerText = invalidCompany.SwiftBIC;
        //    //        row.Cells.Add(cell);
        //    //        cell.InnerText = invalidCompany.IBAN;
        //    //        row.Cells.Add(cell);
        //    //        table.Rows.Add(row);

        //    //using (var sw = new StringWriter())
        //    //{
        //    //    table.RenderControl(new HtmlTextWriter(sw));
        //    //    html = sw.ToString();
        //    //}
        //}
        public void GetMonthlyTotalsReport(DateTime selectedStartDate, DateTime selectedEndDate, EnumReportType reportType)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application oXL;
                Microsoft.Office.Interop.Excel._Workbook oWB;
                Microsoft.Office.Interop.Excel._Worksheet oSheet;
                Microsoft.Office.Interop.Excel.Range oRng;
                object misvalue = System.Reflection.Missing.Value;
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;
                int rowCount = 5;

                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(incomebyMonthQuery, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@startdate", SqlDbType.DateTime));
                        cmd.Parameters.Add(new SqlParameter("@enddate", SqlDbType.DateTime));
                        cmd.Parameters["@startdate"].Value = selectedStartDate;
                        cmd.Parameters["@enddate"].Value = selectedEndDate;
                        SqlDataReader dr = cmd.ExecuteReader();
                        oSheet.Cells[1, 1] = _hotel.name;
                        oSheet.Cells[1, 4] = reportType.ToString();
                        oSheet.Cells[3, 3] = "DateRange from " + selectedStartDate.ToShortDateString() + " to "
                                            + selectedEndDate.ToShortDateString();
                        // Loop through the fields and add headers
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            string name = dr.GetName(i);
                            if (name.Contains(","))
                                name = "\"" + name + "\"";
                            oSheet.Cells[4, i + 1] = name;
                        }
                        //oSheet.Cells[4, dr.FieldCount + 4] = "Room Tax";
                        oSheet.get_Range("A" + 1, "H" + 4).Font.Bold = true;
                        oSheet.get_Range("A1", "H4").VerticalAlignment =
                   Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;



                        // Loop through the rows and output the data
                        while (dr.Read())
                        {

                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string value = dr[i].ToString();
                                if (value.Contains(","))
                                    value = "\"" + value + "\"";
                                oSheet.Cells[rowCount, i + 1] = value;
                                //fs.Write(value + ",");
                            }
                            rowCount++;
                            //oSheet.Cells[rowCount, dr.FieldCount] = roomTax;
                            //fs.WriteLine();
                        }
                        //fs.Close();
                    }

                    oSheet.get_Range("A1", "E100").Columns.AutoFit();
                    oXL.Visible = false;
                    oXL.UserControl = false;
                    oWB.SaveAs(reportGenerationPath + "IncomeByDateRange" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                        false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(oSheet);
                    Marshal.ReleaseComObject(oWB);
                    MessageBox.Show("Report Create at:" + reportGenerationPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error generating DateRange Income:" + Environment.NewLine + ex.Message);
            }
        }


        public void GetTaxExemptionReport(DateTime selectedStartDate, DateTime selectedEndDate, EnumReportType reportType)
        {
            SQLToCSV(taxExemptionsTransaction, @"C:\CCSApps-Temp\Reports\Report.csv", selectedStartDate, selectedEndDate, reportType);
        }

        public void GetRoomRevenueByDateRange(DateTime selectedStartDate, DateTime selectedEndDate, EnumReportType reportType, string roomNumber)
        {
            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel._Workbook oWB;
            Microsoft.Office.Interop.Excel._Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng;
            object misvalue = System.Reflection.Missing.Value;
            oXL = new Microsoft.Office.Interop.Excel.Application();
            oXL.Visible = false;

            //Get a new workbook.
            oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
            oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;
            int rowCount = 5;

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(roomRevenueByDateRange, conn))
                {
                    cmd.Parameters.Add(new SqlParameter("@startdate", SqlDbType.DateTime));
                    cmd.Parameters.Add(new SqlParameter("@enddate", SqlDbType.DateTime));
                    cmd.Parameters.Add(new SqlParameter("@RoomNumber", SqlDbType.NVarChar));
                    cmd.Parameters["@startdate"].Value = selectedStartDate;
                    cmd.Parameters["@enddate"].Value = selectedEndDate;
                    cmd.Parameters["@RoomNumber"].Value = roomNumber;
                    SqlDataReader dr = cmd.ExecuteReader();
                    oSheet.Cells[1, 1] = _hotel.name;
                    oSheet.Cells[1, 4] = roomNumber;

                    oSheet.Cells[3, 3] = "DateRange from " + selectedStartDate.ToShortDateString() + " to "
                                        + selectedEndDate.ToShortDateString();
                    // Loop through the fields and add headers
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        string name = dr.GetName(i);
                        if (name.Contains(","))
                            name = "\"" + name + "\"";
                        oSheet.Cells[4, i + 1] = name;
                    }
                    //oSheet.Cells[4, dr.FieldCount + 4] = "Room Tax";
                    oSheet.get_Range("A" + 1, "H" + 4).Font.Bold = true;
                    oSheet.get_Range("A1", "H4").VerticalAlignment =
               Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;



                    // Loop through the rows and output the data
                    while (dr.Read())
                    {

                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            string value = dr[i].ToString();
                            if (value.Contains(","))
                                value = "\"" + value + "\"";
                            oSheet.Cells[rowCount, i + 1] = value;
                            //fs.Write(value + ",");
                        }
                        rowCount++;
                        //oSheet.Cells[rowCount, dr.FieldCount] = roomTax;
                        //fs.WriteLine();
                    }
                    //fs.Close();
                }

                oSheet.get_Range("A1", "E100").Columns.AutoFit();
                oXL.Visible = false;
                oXL.UserControl = false;
                oWB.SaveAs(reportGenerationPath + "RoomRevenueByDateRange" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                    false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                Marshal.ReleaseComObject(oSheet);
                Marshal.ReleaseComObject(oWB);
                MessageBox.Show("Report Create at:" + reportGenerationPath);
            }
        }

        public void GetMonthlyTotalsByDateRange(DateTime selectedStartDate, DateTime selectedEndDate, EnumReportType reportType)
        {
            SQLToCSV(monthlyReports, @"C:\CCSApps-Temp\Reports\Report.csv", selectedStartDate, selectedEndDate, reportType);
        }

        string sqlConnectionString = ConfigurationManager.ConnectionStrings["HMS"].ConnectionString;
        private List<Floors> _floors = new List<Floors>();
        private List<PaymentModes> _paymentModes = new List<PaymentModes>();
        private List<Proofs> _proofs = new List<Proofs>();
        private List<Rooms> _rooms = new List<Rooms>();
        const string key = "1831457";
        private List<RoomType> _roomTypes;
        private List<Status> _statuses;
        private AppMain _hotel;

        public HMSService()
        {
            _statuses = new List<Status>();
            _hotel = GetHotelDetails().FirstOrDefault();
        }

        public IEnumerable<AppMain> GetHotelDetails()
        {
            using (SqlConnection connection =
            new SqlConnection(sqlConnectionString))
            {
                // Create the Command and Parameter objects.
                using (SqlCommand command = new SqlCommand(hotelNameSQL, connection))
                {

                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        yield return new AppMain
                        {
                            name = reader.GetString(0),
                            DisplayName = reader.GetString(1),
                            Address = reader.GetString(2)
                        };
                    }
                    reader.Close();
                }
            }
        }

        public Arrivals GetArrivals(string roomId)
        {
            var Room = GetRooms().FirstOrDefault(r => r.RoomNumber == roomId);
            Arrivals _arrivals = new Arrivals(new Guests(), Room);
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                // Create the Command and Parameter objects.
                using (SqlCommand command = new SqlCommand(getArrivals, connection))
                {
                    command.Parameters.Add(new SqlParameter("@RoomId", Room.Id));
                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        //_arrivals.RoomId = GetRooms().FirstOrDefault(r => r.Id == reader.GetInt32(0));
                        //_arrivals.GuestId = ;
                        //_arrivals.CheckInDate = reader.IsDBNull(2) ? DateTime.Now : reader.GetDateTime(2);
                        //_arrivals.CheckoutDate = reader.IsDBNull(3) ? DateTime.Now : reader.GetDateTime(3);
                        //_arrivals.NumberofGuests = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                        //_arrivals.NumberofDays = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                        //_arrivals.ModeofPaymentId = GetPaymentModes().FirstOrDefault(p => p.Id == (reader.IsDBNull(6) ? 0 : reader.GetInt32(6)));
                        //_arrivals.GuestRemarks = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
                        //_arrivals.TaxExemption = reader.IsDBNull(9) ? 0 : reader.GetDecimal(9);
                        //_arrivals.RoomCharge = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10);
                        //_arrivals.RoomRate = reader.IsDBNull(12) ? 0 : reader.GetDecimal(12);
                        _arrivals.Guest = GetGuests(reader.GetInt32(0));
                        _arrivals.CheckInDate = reader.IsDBNull(1) ? DateTime.Now : reader.GetDateTime(1);
                        _arrivals.CheckoutDate = reader.IsDBNull(2) ? (DateTime?)null : reader.GetDateTime(2);
                        _arrivals.NumberofGuests = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                        _arrivals.NumberofDays = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                        _arrivals.ModeofPaymentId = GetPaymentModes().FirstOrDefault(p => p.Id == (reader.IsDBNull(5) ? 0 : reader.GetInt32(5))); ;
                        _arrivals.GuestRemarks = reader.IsDBNull(6) ? string.Empty : reader.GetString(6);
                        _arrivals.RoomId = GetRooms().FirstOrDefault(r => r.Id == reader.GetInt32(7));
                        _arrivals.TaxExemption = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8);
                        _arrivals.RoomCharge = reader.IsDBNull(9) ? 0 : reader.GetDecimal(9);
                        _arrivals.RoomRate = reader.IsDBNull(10) ? 0 : reader.GetDecimal(10);
                        _arrivals.OldRoom = reader.IsDBNull(11) ? string.Empty : reader.GetString(11);
                        _arrivals.TransferredRoom = reader.IsDBNull(12) ? string.Empty : reader.GetString(12);
                        _arrivals.ExpiryDate = reader.IsDBNull(13) ? DateTime.Now : reader.GetDateTime(13);
                        _arrivals.CVV = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                        _arrivals.Deposit = reader.IsDBNull(15) ? 0 : reader.GetDecimal(15);
                        _arrivals.CentralGST = reader.IsDBNull(16) ? 0 : reader.GetDecimal(16);
                        _arrivals.StateGST = reader.IsDBNull(17) ? 0 : reader.GetDecimal(17);
                        _arrivals.CardNumber = reader.IsDBNull(18) ? string.Empty : reader.GetString(18);
                        _arrivals.Misc = reader.IsDBNull(19) ? 0 : reader.GetDecimal(19);
                        _arrivals.Balance = reader.IsDBNull(20) ? 0 : reader.GetDecimal(20);
                        _arrivals.TentativeCheckoutDate = reader.IsDBNull(21) ? DateTime.Now : reader.GetDateTime(21);
                        _arrivals.RoomServiceTax = reader.IsDBNull(22) ? 0 : reader.GetDecimal(22);
                        _arrivals.CheckinType = reader.IsDBNull(23) ? "Daily" : reader.GetString(23);
                    }
                    reader.Close();
                }
            }
            return _arrivals;
        }

        public Guests GetGuests(int guestId)
        {
            Guests _guest = new Guests();
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                // Create the Command and Parameter objects.
                using (SqlCommand command = new SqlCommand(getGuests, connection))
                {
                    command.Parameters.Add(new SqlParameter("@guestId", guestId));
                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        //_guest.Id = reader.GetInt32(0);
                        //_guest.FullName = reader.GetString(1);
                        //_guest.DateOfBirth = reader.IsDBNull(2) ? DateTime.Now : reader.GetDateTime(2);
                        //_guest.Address = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                        //_guest.IdProofType = GetProofTypes().FirstOrDefault(p => p.Id == (reader.IsDBNull(4) ? 0 : reader.GetInt32(4))).Description;
                        //_guest.IdProofNumber = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
                        //_guest.CarModel = reader.IsDBNull(6) ? string.Empty : reader.GetString(6);
                        //_guest.RTGNumber = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);

                        _guest.Id = reader.GetInt32(0);
                        _guest.FullName = reader.GetString(1);
                        _guest.DateOfBirth = reader.IsDBNull(2) ? DateTime.Now : reader.GetDateTime(2);
                        _guest.Address = reader.IsDBNull(3) ? String.Empty : reader.GetString(3);
                        _guest.IdProofType = GetProofTypes().FirstOrDefault(p => p.Code == (reader.IsDBNull(4) ? string.Empty : reader.GetString(4)))?.Description;
                        _guest.IdProofNumber = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
                        _guest.CarModel = reader.IsDBNull(6) ? string.Empty : reader.GetString(6);
                        _guest.RTGNumber = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
                        _guest.Gender = reader.IsDBNull(8) ? string.Empty : reader.GetString(8);
                        _guest.Zipcode = reader.IsDBNull(9) ? string.Empty : reader.GetString(9);
                        _guest.City = reader.IsDBNull(10) ? string.Empty : reader.GetString(10);
                        _guest.State = reader.IsDBNull(11) ? string.Empty : reader.GetString(11);
                        _guest.Phonenumber = reader.IsDBNull(12) ? string.Empty : reader.GetString(12);
                        _guest.Emailid = reader.IsDBNull(13) ? string.Empty : reader.GetString(13);
                        _guest.MoreIds = reader.IsDBNull(14) ? string.Empty : reader.GetString(14);
                        _guest.Purposeofvisit = reader.IsDBNull(15) ? string.Empty : reader.GetString(15);
                        _guest.Company = reader.IsDBNull(16) ? string.Empty : reader.GetString(16);
                    }
                    reader.Close();
                }
            }
            return _guest;
        }

        public Rooms GetRoomDetails(string roomNumber)
        {
            return GetRooms().FirstOrDefault(f => f.RoomNumber == roomNumber);
        }

        public List<Floors> GetFloors()
        {
            if (!_floors.Any())
            {
                using (SqlConnection connection =
                new SqlConnection(sqlConnectionString))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand(getFloors, connection);

                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        _floors.Add(new Floors
                        {
                            Id = reader.GetInt32(0),
                            floor_name = reader.GetString(1)
                        });
                    }
                    reader.Close();
                }
            }
            return _floors;
        }

        public List<PaymentModes> GetPaymentModes()
        {
            if (!_paymentModes.Any())
            {
                using (SqlConnection connection =
                new SqlConnection(sqlConnectionString))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand(getModesofPayment, connection);

                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        _paymentModes.Add(new PaymentModes
                        {
                            Id = reader.GetInt32(0),
                            Code = reader.GetString(1),
                            Description = reader.GetString(2)
                        });
                    }
                    reader.Close();
                }
            }
            return _paymentModes;
        }

        public List<Status> GetStatuses()
        {
            if (!_statuses.Any())
            {
                using (SqlConnection connection =
                new SqlConnection(sqlConnectionString))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand(getStatuses, connection);

                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        _statuses.Add(new Status()
                        {
                            Id = reader.GetInt32(0),
                            Code = reader.GetString(1),
                            Description = reader.GetString(2)
                        });
                    }
                    reader.Close();
                }
            }
            return _statuses;
        }

        public List<Proofs> GetProofTypes()
        {
            if (!_proofs.Any())
            {
                using (SqlConnection connection =
                new SqlConnection(sqlConnectionString))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand(getProofs, connection);

                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        _proofs.Add(new Proofs
                        {
                            Id = reader.GetInt32(0),
                            Code = reader.GetString(1),
                            Description = reader.GetString(2)
                        });
                    }
                    reader.Close();
                }
            }
            return _proofs;
        }

        public IEnumerable<Rooms> GetRooms()
        {
            _rooms.Clear();
            using (SqlConnection connection =
        new SqlConnection(sqlConnectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(getRooms, connection);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.

                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    _rooms.Add(new Rooms
                    {
                        Id = reader.GetInt32(0),
                        FloorId = reader.GetInt32(1),
                        RoomNumber = reader.GetInt32(2).ToString(),
                        Room_Display = reader.GetString(3),
                        StatusId = reader.IsDBNull(4) ? 0 : reader.GetInt32(4),
                        RoomType = GetRoomTypes().FirstOrDefault(f => f.Id == reader.GetInt32(5))
                    });
                }
                reader.Close();

            }

            return _rooms;
        }


        public string CheckUserAccess(string inputUserName, string inputPassWord)
        {
            if (!string.IsNullOrWhiteSpace(inputUserName))
            {
                //XTea xTea = new XTea();
                string userName = string.Empty;
                string password = string.Empty;
                using (SqlConnection connection =
                new SqlConnection(sqlConnectionString))
                {
                    // Create the Command and Parameter objects.
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.Parameters.AddWithValue("@username", inputUserName);

                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            userName = reader[0].ToString();
                            password = reader[1].ToString();
                        }
                        reader.Close();
                        if (userName != inputUserName)
                        {
                            return "No userExist";
                        }
                        else if (userName == inputUserName && password == inputPassWord)
                        {
                            return "Success";
                        }
                        else if (userName == inputUserName && password != inputPassWord)
                        {
                            return "Incorrect Password";
                        }
                    }
                    catch (Exception ex)
                    {
                        return "Fail";
                    }
                    return string.Empty;
                }
            }
            return "Please enter username and password";
        }
        //TODO - make file name configurable
        private void SQLToCSVRoomTaxCollection(string query, string Filename, DateTime startDate, DateTime endDate, EnumReportType reportType)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application oXL;
                Microsoft.Office.Interop.Excel._Workbook oWB;
                Microsoft.Office.Interop.Excel._Worksheet oSheet;
                Microsoft.Office.Interop.Excel.Range oRng;
                object misvalue = System.Reflection.Missing.Value;
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;
                int rowCount = 5;
                decimal totalTax = 0;
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@startdate", SqlDbType.DateTime));
                        cmd.Parameters.Add(new SqlParameter("@enddate", SqlDbType.DateTime));
                        cmd.Parameters["@startdate"].Value = startDate;
                        cmd.Parameters["@enddate"].Value = endDate;
                        SqlDataReader dr = cmd.ExecuteReader();

                        oSheet.Cells[1, 1] = _hotel.name;
                        oSheet.Cells[1, 4] = reportType.ToString();
                        oSheet.Cells[3, 3] = "DateRange from " + startDate.ToShortDateString() + " to "
                                            + endDate.ToShortDateString();
                        // Loop through the fields and add headers
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            string name = dr.GetName(i);
                            if (name.Contains(","))
                                name = "\"" + name + "\"";
                            oSheet.Cells[4, i + 1] = name;
                        }
                        //oSheet.Cells[4, dr.FieldCount + 4] = "Room Tax";
                        oSheet.get_Range("A" + 1, "E" + 4).Font.Bold = true;
                        oSheet.get_Range("A1", "F4").VerticalAlignment =
                   Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;



                        // Loop through the rows and output the data
                        while (dr.Read())
                        {

                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string value = dr[i].ToString();
                                if (value.Contains(","))
                                    value = "\"" + value + "\"";
                                oSheet.Cells[rowCount, i + 1] = value;
                                if (i == 4)
                                    totalTax = totalTax + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                //fs.Write(value + ",");
                            }
                            rowCount++;
                            //oSheet.Cells[rowCount, dr.FieldCount] = roomTax;
                            //fs.WriteLine();
                        }
                        //fs.Close();
                    }
                    oSheet.Cells[rowCount + 2, 4] = "Total Tax Collected";
                    oSheet.get_Range("D" + rowCount + 7).Font.Bold = true;

                    oSheet.Cells[rowCount + 2, 5] = totalTax;
                    oSheet.get_Range("A1", "E100").Columns.AutoFit();
                    oXL.Visible = false;
                    oXL.UserControl = false;
                    oWB.SaveAs(reportGenerationPath + "RoomTaxCollection" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                        false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(oSheet);
                    Marshal.ReleaseComObject(oWB);
                    MessageBox.Show("Report Create at:" + reportGenerationPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error generating report" + ex.Message);
            }
        }

        private void SQLToCSV(string query, string Filename, DateTime startDate, DateTime endDate, EnumReportType reportType)
        {
            try
            {

                Microsoft.Office.Interop.Excel.Application oXL;
                Microsoft.Office.Interop.Excel._Workbook oWB;
                Microsoft.Office.Interop.Excel._Worksheet oSheet;
                Microsoft.Office.Interop.Excel.Range oRng;
                object misvalue = System.Reflection.Missing.Value;
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(""));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;
                int rowCount = 5;
                decimal totalTax = 0;
                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.Add(new SqlParameter("@startdate", SqlDbType.DateTime));
                        cmd.Parameters.Add(new SqlParameter("@enddate", SqlDbType.DateTime));
                        cmd.Parameters["@startdate"].Value = startDate;
                        cmd.Parameters["@enddate"].Value = endDate;
                        SqlDataReader dr = cmd.ExecuteReader();
                        oSheet.Cells[1, 1] = _hotel.name;
                        oSheet.Cells[1, 4] = reportType.ToString();
                        oSheet.Cells[3, 3] = "DateRange from " + startDate.ToShortDateString() + " to "
                                            + endDate.ToShortDateString();
                        // Loop through the fields and add headers
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            string name = dr.GetName(i);
                            if (name.Contains(","))
                                name = "\"" + name + "\"";
                            oSheet.Cells[4, i + 1] = name;
                        }
                        //oSheet.Cells[4, dr.FieldCount + 4] = "Room Tax";
                        oSheet.get_Range("A" + 1, "E" + 4).Font.Bold = true;
                        oSheet.get_Range("A1", "F4").VerticalAlignment =
                   Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;



                        // Loop through the rows and output the data
                        while (dr.Read())
                        {

                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string value = dr[i].ToString();
                                if (value.Contains(","))
                                    value = "\"" + value + "\"";
                                oSheet.Cells[rowCount, i + 1] = value;
                                if (i == 4)
                                    totalTax = totalTax + (string.IsNullOrWhiteSpace(value) ? 0 : Convert.ToDecimal(value));
                                //fs.Write(value + ",");
                            }
                            rowCount++;
                            //oSheet.Cells[rowCount, dr.FieldCount] = roomTax;
                            //fs.WriteLine();
                        }
                        //fs.Close();
                    }
                    oSheet.Cells[rowCount + 2, 4] = "Total Tax Collected";
                    oSheet.get_Range("D" + rowCount + 7).Font.Bold = true;

                    oSheet.Cells[rowCount + 2, 5] = totalTax;
                    oSheet.get_Range("A1", "E100").Columns.AutoFit();
                    oXL.Visible = false;
                    oXL.UserControl = false;
                    oWB.SaveAs(reportGenerationPath + "Report" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                        false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    Marshal.ReleaseComObject(oSheet);
                    Marshal.ReleaseComObject(oWB);
                    MessageBox.Show("Report Create at:" + reportGenerationPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error generating report." + ex.Message);
            }
        }

        public List<RoomType> GetRoomTypes()
        {
            _roomTypes = new List<RoomType>();
            using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            {
                // Create the Command and Parameter objects.
                using (SqlCommand command = new SqlCommand(getRoomTypes, connection))
                {
                    // Open the connection in a try/catch block. 
                    // Create and execute the DataReader, writing the result
                    // set to the console window.

                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        RoomType roomType = new RoomType();
                        roomType.Id = reader.GetInt32(0);
                        roomType.Description = reader.GetString(1);
                        roomType.Code = reader.GetString(2);
                        roomType.RoomCharge = reader.GetDecimal(3);
                        roomType.CentralGST = reader.GetDecimal(4);
                        roomType.StateGST = reader.GetDecimal(5);
                        roomType.RoomServiceTax = reader.GetDecimal(6);
                        _roomTypes.Add(roomType);
                    }
                    reader.Close();
                }
            }
            return _roomTypes;
        }

        public void GetDailyReportsforRoomAndTax(DateTime startDate, DateTime endDate, EnumReportType reportType)
        {
            SQLToCSVRoomTaxCollection(dailyRoomPlusTax, @"C:\CCSApps-Temp\Reports\Report.csv", startDate, endDate, reportType);
        }

        public void AddUser(string userName, string password, string fullName, int roleId)
        {
            try
            {
                using (SqlConnection connection =
                           new SqlConnection(sqlConnectionString))
                {

                    using (SqlCommand command = new SqlCommand(InsertAUser, connection))
                    {
                        //XTea xTea = new XTea();
                        //var encryptedPassword = xTea.EncryptString(password, key);
                        command.Parameters.AddWithValue("@username", userName);
                        command.Parameters.AddWithValue("@password", password);
                        command.Parameters.AddWithValue("@fullname", fullName);
                        command.Parameters.AddWithValue("@roleid", roleId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
                MessageBox.Show("User added Successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error adding user:" + ex.Message);
            }
        }
        public void UpdateStatusOfRoom(int roomId, int statusId)
        {
            try
            {
                using (SqlConnection connection =
                           new SqlConnection(sqlConnectionString))
                {

                    using (SqlCommand command = new SqlCommand(updateStatusofRoom, connection))
                    {
                        command.Parameters.AddWithValue("@RoomId", roomId);
                        command.Parameters.AddWithValue("@StatusId", statusId);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }

                }
                MessageBox.Show("Successful");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Updating room id " + roomId + ". Please Retry. Error is:" + ex.Message + Environment.NewLine + ex.InnerException?.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void InsertRoomType(RoomType roomType)
        {
            try
            {
                using (SqlConnection connection =
                           new SqlConnection(sqlConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(insertRoomType, connection))
                    {
                        command.Parameters.AddWithValue("@code", roomType.Code);
                        command.Parameters.AddWithValue("@description", roomType.Description);
                        command.Parameters.AddWithValue("@RoomCharge", roomType.RoomCharge);
                        command.Parameters.AddWithValue("@CentralGST", roomType.CentralGST);
                        command.Parameters.AddWithValue("@StateGST", roomType.StateGST);
                        command.Parameters.AddWithValue("@ServiceTax", roomType.RoomServiceTax);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Updating room type " + roomType.Description + ". Please Retry. Error is:" + ex.Message + Environment.NewLine + ex.InnerException?.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void UpdateRoomType(RoomType roomType)
        {
            try
            {
                using (SqlConnection connection =
                           new SqlConnection(sqlConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(updateRoomType, connection))
                    {
                        command.Parameters.AddWithValue("@description", roomType.Description);
                        command.Parameters.AddWithValue("@RoomCharge", roomType.RoomCharge);
                        command.Parameters.AddWithValue("@CentralGST", roomType.CentralGST);
                        command.Parameters.AddWithValue("@StateGST", roomType.StateGST);
                        command.Parameters.AddWithValue("@ServiceTax", roomType.RoomServiceTax);
                        command.Parameters.AddWithValue("@id", roomType.Id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Updating room type " + roomType.Description + ". Please Retry. Error is:" + ex.Message + Environment.NewLine + ex.InnerException?.Message + Environment.NewLine + ex.StackTrace);
            }
        }
        public void UpdateRoom(Rooms room)
        {
            try
            {
                using (SqlConnection connection =
                           new SqlConnection(sqlConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(updateRoomDetails, connection))
                    {
                        command.Parameters.AddWithValue("@floorid", room.FloorId);
                        command.Parameters.AddWithValue("@roomno", room.RoomNumber);
                        command.Parameters.AddWithValue("@roomdisplay", room.Room_Display);
                        command.Parameters.AddWithValue("@roomtype", room.RoomType.Id);
                        command.Parameters.AddWithValue("@id", room.Id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Updating Room" + room.Room_Display + ". Please Retry. Error is:" + ex.Message + Environment.NewLine + ex.InnerException?.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void InsertRoom(Rooms room)
        {
            try
            {
                using (SqlConnection connection =
                           new SqlConnection(sqlConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(insertRoomDetails, connection))
                    {
                        command.Parameters.AddWithValue("@floorid", room.FloorId);
                        command.Parameters.AddWithValue("@roomno", room.RoomNumber);
                        command.Parameters.AddWithValue("@roomdisplay", room.Room_Display);
                        command.Parameters.AddWithValue("@roomtype", room.RoomType.Id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error adding a new Room" + room.Room_Display + ". Please Retry. Error is:" + ex.Message + Environment.NewLine + ex.InnerException?.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void UpdateFinalDetails(Arrivals arrival, decimal? RoomCharge, decimal? ServiceTax, decimal? CentralGST, decimal? StateGST)
        {
            try
            {
                using (SqlConnection connection =
                           new SqlConnection(sqlConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(updateArrivalDetails, connection))
                    {
                        command.Parameters.AddWithValue("@CheckInDate", arrival.CheckInDate);
                        command.Parameters.AddWithValue("@CheckoutDate", arrival.CheckoutDate ?? SqlDateTime.Null);
                        command.Parameters.AddWithValue("@NumberofGuests", arrival.NumberofGuests);
                        command.Parameters.AddWithValue("@NumberofDays", arrival.NumberofDays);

                        command.Parameters.AddWithValue("@taxExemption", arrival.TaxExemption);
                        command.Parameters.AddWithValue("@RoomCharge", RoomCharge);
                        command.Parameters.AddWithValue("@RoomRate", arrival.RoomRate);
                        command.Parameters.AddWithValue("@oldroom", arrival.OldRoom);
                        command.Parameters.AddWithValue("@deposit", arrival.Deposit);
                        command.Parameters.AddWithValue("@CGST", CentralGST);
                        command.Parameters.AddWithValue("@SGST", StateGST);
                        command.Parameters.AddWithValue("@misc", arrival.Misc);
                        command.Parameters.AddWithValue("@balance", arrival.Balance);
                        command.Parameters.AddWithValue("@TentativeCheckout", arrival.TentativeCheckoutDate);
                        command.Parameters.AddWithValue("@ServiceTax", ServiceTax);
                        command.Parameters.AddWithValue("@CheckinType", arrival.CheckinType);
                        command.Parameters.AddWithValue("@GuestId", arrival.Guest?.Id);
                        command.Parameters.AddWithValue("@RoomId", arrival.RoomId?.Id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Extending the Room. Please Retry. Error is:" + ex.Message + Environment.NewLine + ex.InnerException?.Message + Environment.NewLine + ex.StackTrace);
            }
            try
            {
                var imageString = ConfigurationManager.AppSettings["LogoImage"];
                var xlApp = new Microsoft.Office.Interop.Excel.Application();
                Workbook xlWorkBook = xlApp.Workbooks.Add();
                Worksheet xlWorkSheet = xlWorkBook.Sheets[1];
                xlWorkSheet.get_Range("A" + 1, "D" + 12).Font.Bold = true;
                xlWorkSheet.get_Range("A" + 1, "F" + 4).Font.Bold = true;
                xlWorkSheet.get_Range("A12", "D21").BorderAround();
                xlWorkSheet.Cells[1, 3] = "HOTEL BILL";
                xlWorkSheet.Cells[3, 3] = "HOTEL NAME:";
                xlWorkSheet.Cells[3, 4] = _hotel.name;
                xlWorkSheet.Cells[3, 5] = "DATE:";
                xlWorkSheet.Cells[3, 6] = DateTime.Now.ToString();

                xlWorkSheet.Cells[4, 3] = "ADDRESS:";
                xlWorkSheet.Cells[4, 4] = _hotel.Address;
                xlWorkSheet.Cells[4, 5] = "#INVOICE:";
                xlWorkSheet.Cells[4, 6] = arrival.Guest?.FullName.Substring(0, 3) + arrival.CheckInDate.ToString("ddMMyyyyhhmmss");

                xlWorkSheet.Cells[6, 1] = "BILL TO";
                xlWorkSheet.Cells[7, 1] = "Name of the Guest:";
                xlWorkSheet.Cells[7, 2] = arrival.Guest.FullName;
                xlWorkSheet.Cells[7, 4] = "Number of days";
                xlWorkSheet.Cells[7, 5] = arrival.NumberofDays;

                xlWorkSheet.Cells[8, 1] = "Address:";
                xlWorkSheet.Cells[8, 2] = arrival.Guest?.Address;
                xlWorkSheet.Cells[8, 4] = "Number of guests";
                xlWorkSheet.Cells[8, 5] = arrival.NumberofGuests;

                xlWorkSheet.Cells[9, 1] = "Phone Number:";
                xlWorkSheet.Cells[9, 2] = arrival.Guest.Phonenumber;
                xlWorkSheet.Cells[9, 4] = "Room Number";
                xlWorkSheet.Cells[9, 5] = arrival.RoomId?.Room_Display;

                xlWorkSheet.Cells[10, 1] = "Name of the Company:";
                xlWorkSheet.Cells[10, 2] = arrival.Guest?.Company;
                xlWorkSheet.Cells[10, 4] = "Room Type";
                xlWorkSheet.Cells[10, 5] = arrival.RoomId?.RoomType?.Description;

                xlWorkSheet.Cells[12, 1] = "ROOM #";
                xlWorkSheet.Cells[12, 2] = "CHECK IN DATE";
                xlWorkSheet.Cells[12, 3] = "CHECK OUT DATE";
                xlWorkSheet.Cells[12, 4] = "ROOM CHARGE";

                xlWorkSheet.Cells[13, 1] = arrival.RoomId?.Room_Display;
                xlWorkSheet.Cells[13, 2] = arrival.CheckInDate;
                xlWorkSheet.Cells[13, 3] = arrival.CheckoutDate;
                xlWorkSheet.Cells[13, 4] = RoomCharge;

                xlWorkSheet.Cells[16, 3] = "Deposit";
                xlWorkSheet.Cells[17, 3] = "Central Tax";
                xlWorkSheet.Cells[18, 3] = "State Tax";
                xlWorkSheet.Cells[19, 3] = "Tax Exemption";

                xlWorkSheet.Cells[16, 4] = arrival.Deposit;
                xlWorkSheet.Cells[17, 4] = CentralGST;
                xlWorkSheet.Cells[18, 4] = StateGST;
                xlWorkSheet.Cells[19, 4] = arrival.TaxExemption;

                xlWorkSheet.Cells[21, 3] = "$Total";
                xlWorkSheet.Cells[21, 4] = arrival.RoomCharge + CentralGST + StateGST + arrival.TaxExemption;
                xlWorkSheet.get_Range("A1", "H100").Columns.AutoFit();
                xlWorkBook.SaveAs(reportGenerationPath + "Bill_+" + arrival.Guest?.FullName.Substring(0, 3) + arrival.CheckInDate.ToString("ddMMyyyyhhmmss") + "+.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                xlWorkBook.Close(true);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlApp);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Updating the final details before print out:" + Environment.NewLine + ex.StackTrace);
            }
        }

        public void ExtendTime(Arrivals arrival)
        {
            try
            {
                using (SqlConnection connection =
                           new SqlConnection(sqlConnectionString))
                {
                    using (SqlCommand command = new SqlCommand(extendCheckinTime, connection))
                    {
                        command.Parameters.AddWithValue("@CheckoutDate", arrival.CheckoutDate ?? arrival.TentativeCheckoutDate);
                        command.Parameters.AddWithValue("@TentativeCheckout", arrival.TentativeCheckoutDate);
                        command.Parameters.AddWithValue("@Guestid", arrival.Guest?.Id);
                        command.Parameters.AddWithValue("@NumberofDays", arrival.NumberofDays);
                        command.Parameters.AddWithValue("@RoomId", arrival.RoomId?.Id);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Extending the Room. Please Retry. Error is:" + ex.Message + Environment.NewLine + ex.InnerException?.Message + Environment.NewLine + ex.StackTrace);
            }
            UpdateFinalDetails(arrival, arrival.RoomCharge, arrival.RoomServiceTax, arrival.CentralGST, arrival.StateGST);
        }
    }
}