USE [HMSystem]
GO
ALTER TABLE [dbo].[rooms] DROP CONSTRAINT [FK__rooms__ROOM_TYPE__46E78A0C]
GO
ALTER TABLE [dbo].[Arrivals] DROP CONSTRAINT [FK__Arrivals__RoomId__45F365D3]
GO
ALTER TABLE [dbo].[Arrivals] DROP CONSTRAINT [FK__Arrivals__GuestI__44FF419A]
GO
/****** Object:  Table [dbo].[users]    Script Date: 1/1/2019 12:21:30 PM ******/
DROP TABLE [dbo].[users]
GO
/****** Object:  Table [dbo].[shifts]    Script Date: 1/1/2019 12:21:30 PM ******/
DROP TABLE [dbo].[shifts]
GO
/****** Object:  Table [dbo].[shiftmanagements]    Script Date: 1/1/2019 12:21:30 PM ******/
DROP TABLE [dbo].[shiftmanagements]
GO
/****** Object:  Table [dbo].[rooms]    Script Date: 1/1/2019 12:21:30 PM ******/
DROP TABLE [dbo].[rooms]
GO
/****** Object:  Table [dbo].[ModesofPayment]    Script Date: 1/1/2019 12:21:30 PM ******/
DROP TABLE [dbo].[ModesofPayment]
GO
/****** Object:  Table [dbo].[LookupStatus]    Script Date: 1/1/2019 12:21:30 PM ******/
DROP TABLE [dbo].[LookupStatus]
GO
/****** Object:  Table [dbo].[LookupRoomType]    Script Date: 1/1/2019 12:21:30 PM ******/
DROP TABLE [dbo].[LookupRoomType]
GO
/****** Object:  Table [dbo].[LookupProofs]    Script Date: 1/1/2019 12:21:31 PM ******/
DROP TABLE [dbo].[LookupProofs]
GO
/****** Object:  Table [dbo].[Guests]    Script Date: 1/1/2019 12:21:31 PM ******/
DROP TABLE [dbo].[Guests]
GO
/****** Object:  Table [dbo].[floors]    Script Date: 1/1/2019 12:21:31 PM ******/
DROP TABLE [dbo].[floors]
GO
/****** Object:  Table [dbo].[Arrivals]    Script Date: 1/1/2019 12:21:31 PM ******/
DROP TABLE [dbo].[Arrivals]
GO
/****** Object:  Table [dbo].[AppMain]    Script Date: 1/1/2019 12:21:31 PM ******/
DROP TABLE [dbo].[AppMain]
GO
USE [master]
GO
/****** Object:  Database [HMSystem]    Script Date: 1/1/2019 12:21:31 PM ******/
DROP DATABASE [HMSystem]
GO
/****** Object:  Database [HMSystem]    Script Date: 1/1/2019 12:21:31 PM ******/
CREATE DATABASE [HMSystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'HMSystem', FILENAME = N'C:\Users\rkorimilli\HMSystem.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'HMSystem_log', FILENAME = N'C:\Users\rkorimilli\HMSystem_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HMSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HMSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HMSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HMSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HMSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HMSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [HMSystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HMSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HMSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HMSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HMSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HMSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HMSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HMSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HMSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HMSystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [HMSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HMSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HMSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HMSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HMSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HMSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HMSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HMSystem] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [HMSystem] SET  MULTI_USER 
GO
ALTER DATABASE [HMSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HMSystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HMSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HMSystem] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [HMSystem] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [HMSystem] SET QUERY_STORE = OFF
GO
USE [HMSystem]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [HMSystem]
GO
/****** Object:  Table [dbo].[AppMain]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppMain](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NULL,
	[displayname] [nvarchar](200) NULL,
	[Address] [varchar](1000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Arrivals]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Arrivals](
	[GuestId] [int] NULL,
	[CheckInDate] [datetime] NULL,
	[CheckoutDate] [datetime] NULL,
	[NumberofGuests] [int] NULL,
	[NumberofDays] [int] NULL,
	[ModeofPaymentId] [int] NULL,
	[GuestRemarks] [varchar](500) NULL,
	[RoomId] [int] NULL,
	[taxExemption] [decimal](18, 0) NULL,
	[RoomCharge] [decimal](18, 0) NULL,
	[RoomRate] [decimal](18, 0) NULL,
	[oldroom] [nvarchar](20) NULL,
	[transferredroom] [nvarchar](20) NULL,
	[expiredate] [datetime] NULL,
	[CVV] [int] NULL,
	[deposit] [decimal](18, 0) NULL,
	[CGST] [decimal](18, 0) NULL,
	[SGST] [decimal](18, 0) NULL,
	[cardnumber] [nvarchar](50) NULL,
	[misc] [decimal](18, 0) NULL,
	[balance] [decimal](18, 0) NULL,
	[creation_date] [datetime] NULL,
	[TentativeCheckout] [datetime] NULL,
	[ServiceTax] [decimal](18, 0) NULL,
	[CheckinType] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[floors]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[floors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[floor_name] [nvarchar](200) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Guests]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Guests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [varchar](500) NULL,
	[DateOfBirth] [datetime] NULL,
	[Address] [varchar](1000) NULL,
	[IdProofId] [nvarchar](30) NULL,
	[IdProofNumber] [varchar](200) NULL,
	[CarModel] [varchar](200) NULL,
	[RTGNum] [varchar](200) NULL,
	[gender] [nvarchar](10) NULL,
	[zipcode] [nvarchar](20) NULL,
	[city] [nvarchar](50) NULL,
	[state] [nvarchar](50) NULL,
	[phonenumber] [nvarchar](30) NULL,
	[emailid] [nvarchar](100) NULL,
	[MoreIds] [nvarchar](50) NULL,
	[purposeofvisit] [nvarchar](100) NULL,
	[company] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LookupProofs]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LookupProofs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[description] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LookupRoomType]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LookupRoomType](
	[id] [int] NOT NULL,
	[description] [varchar](50) NULL,
	[code] [varchar](10) NULL,
	[RoomCharge] [decimal](18, 0) NULL,
	[CentralGST] [decimal](18, 0) NULL,
	[StateGST] [decimal](18, 0) NULL,
	[ServiceTax] [decimal](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LookupStatus]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LookupStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[description] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ModesofPayment]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ModesofPayment](
	[code] [varchar](50) NULL,
	[description] [varchar](50) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rooms]    Script Date: 1/1/2019 12:21:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rooms](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[floor_id] [int] NULL,
	[room_no] [int] NULL,
	[room_display] [nvarchar](100) NULL,
	[status] [int] NULL,
	[ROOM_TYPE] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[shiftmanagements]    Script Date: 1/1/2019 12:21:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[shiftmanagements](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[ShiftName] [nvarchar](50) NULL,
	[ShiftStartTime] [datetime] NULL,
	[ShiftEndTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[shifts]    Script Date: 1/1/2019 12:21:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[shifts](
	[userid] [int] NULL,
	[shiftname] [varchar](50) NULL,
	[logindate] [datetime] NULL,
	[logoutdate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[users]    Script Date: 1/1/2019 12:21:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](100) NULL,
	[fullname] [varchar](100) NULL,
	[RoleId] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AppMain] ON 

INSERT [dbo].[AppMain] ([id], [name], [displayname], [Address]) VALUES (1, N'Hotel Lemon Tree', N'Lemon Tree', N'Hitech City, 
 Madhapur,
 Hyderabad')
SET IDENTITY_INSERT [dbo].[AppMain] OFF
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (1, CAST(N'2018-09-06T23:26:26.787' AS DateTime), CAST(N'2018-11-21T21:31:17.260' AS DateTime), 3, NULL, 1, N'TEST', 1, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-11-10T12:27:35.067' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (2, CAST(N'2018-09-05T23:26:26.787' AS DateTime), CAST(N'2018-11-21T22:13:27.947' AS DateTime), 3, NULL, 1, N'TEST', 9, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-05T12:27:46.287' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (3, CAST(N'2018-09-04T23:26:26.790' AS DateTime), CAST(N'2018-12-07T16:09:31.343' AS DateTime), 3, NULL, 1, N'TEST', 13, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-08-18T12:27:52.580' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (4, CAST(N'2018-09-03T23:26:26.790' AS DateTime), CAST(N'2018-11-28T20:53:57.170' AS DateTime), 3, NULL, 1, N'TEST', 16, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-10-12T12:27:58.387' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (5, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-11-28T21:02:15.377' AS DateTime), 3, NULL, 1, N'TEST', 22, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-07-14T12:28:10.380' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (6, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-12-08T20:36:10.670' AS DateTime), 3, NULL, 1, N'TEST', 23, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (7, CAST(N'2018-09-03T23:26:26.800' AS DateTime), CAST(N'2018-12-07T16:43:28.423' AS DateTime), 3, NULL, 1, N'TEST', 28, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-01-15T12:28:32.630' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (8, CAST(N'2018-09-03T23:26:26.800' AS DateTime), CAST(N'2018-12-07T16:43:39.327' AS DateTime), 3, NULL, 1, N'TEST', 29, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2017-12-20T12:28:39.927' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (1, CAST(N'2018-09-06T23:26:26.787' AS DateTime), CAST(N'2018-11-21T21:31:17.260' AS DateTime), 3, NULL, 1, N'TEST', 1, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-11-10T12:27:35.067' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (2, CAST(N'2018-09-05T23:26:26.787' AS DateTime), CAST(N'2018-11-21T22:13:27.947' AS DateTime), 3, NULL, 1, N'TEST', 9, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-05T12:27:46.287' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (3, CAST(N'2018-09-04T23:26:26.790' AS DateTime), CAST(N'2018-12-07T16:09:31.343' AS DateTime), 3, NULL, 1, N'TEST', 13, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-08-18T12:27:52.580' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (4, CAST(N'2018-09-03T23:26:26.790' AS DateTime), CAST(N'2018-11-28T20:53:57.170' AS DateTime), 3, NULL, 1, N'TEST', 16, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-10-12T12:27:58.387' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (5, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-11-28T21:02:15.377' AS DateTime), 3, NULL, 1, N'TEST', 22, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-07-14T12:28:10.380' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (6, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-12-08T20:36:10.670' AS DateTime), 3, NULL, 1, N'TEST', 23, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (7, CAST(N'2018-09-03T23:26:26.800' AS DateTime), CAST(N'2018-12-07T16:43:28.423' AS DateTime), 3, NULL, 1, N'TEST', 28, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-01-15T12:28:32.630' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (8, CAST(N'2018-09-03T23:26:26.800' AS DateTime), CAST(N'2018-12-07T16:43:39.327' AS DateTime), 3, NULL, 1, N'TEST', 29, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2017-12-20T12:28:39.927' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (1, CAST(N'2018-09-06T23:26:26.787' AS DateTime), CAST(N'2018-11-21T21:31:17.260' AS DateTime), 3, NULL, 1, N'TEST', 1, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (2, CAST(N'2018-09-05T23:26:26.787' AS DateTime), CAST(N'2018-11-21T22:13:27.947' AS DateTime), 3, NULL, 1, N'TEST', 9, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (3, CAST(N'2018-09-04T23:26:26.790' AS DateTime), CAST(N'2018-12-07T16:09:31.343' AS DateTime), 3, NULL, 1, N'TEST', 13, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (4, CAST(N'2018-09-03T23:26:26.790' AS DateTime), CAST(N'2018-11-28T20:53:57.170' AS DateTime), 3, NULL, 1, N'TEST', 16, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (5, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-11-28T21:02:15.377' AS DateTime), 3, NULL, 1, N'TEST', 22, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (6, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-12-08T20:36:10.670' AS DateTime), 3, NULL, 1, N'TEST', 23, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (7, CAST(N'2018-09-03T23:26:26.800' AS DateTime), CAST(N'2018-12-07T16:43:28.423' AS DateTime), 3, NULL, 1, N'TEST', 28, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (8, CAST(N'2018-09-03T23:26:26.800' AS DateTime), CAST(N'2018-12-07T16:43:39.327' AS DateTime), 3, NULL, 1, N'TEST', 29, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (1, CAST(N'2018-09-06T23:26:26.787' AS DateTime), CAST(N'2018-11-21T21:31:17.260' AS DateTime), 3, NULL, 1, N'TEST', 1, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (2, CAST(N'2018-09-05T23:26:26.787' AS DateTime), CAST(N'2018-11-21T22:13:27.947' AS DateTime), 3, NULL, 1, N'TEST', 9, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (3, CAST(N'2018-09-04T23:26:26.790' AS DateTime), CAST(N'2018-12-07T16:09:31.343' AS DateTime), 3, NULL, 1, N'TEST', 13, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (4, CAST(N'2018-09-03T23:26:26.790' AS DateTime), CAST(N'2018-11-28T20:53:57.170' AS DateTime), 3, NULL, 1, N'TEST', 16, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (5, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-11-28T21:02:15.377' AS DateTime), 3, NULL, 1, N'TEST', 22, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (6, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-12-08T20:36:10.670' AS DateTime), 3, NULL, 1, N'TEST', 23, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (7, CAST(N'2018-09-03T23:26:26.800' AS DateTime), CAST(N'2018-12-07T16:43:28.423' AS DateTime), 3, NULL, 1, N'TEST', 28, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (8, CAST(N'2018-09-03T23:26:26.800' AS DateTime), CAST(N'2018-12-07T16:43:39.327' AS DateTime), 3, NULL, 1, N'TEST', 29, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), NULL, NULL, NULL, NULL, NULL, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), NULL, CAST(200 AS Decimal(18, 0)), NULL, CAST(N'2018-09-02T14:14:48.207' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (13, CAST(N'2018-11-19T23:40:26.067' AS DateTime), CAST(N'2018-11-21T21:59:02.747' AS DateTime), 0, 1, 2, N'sdhbjhgdf', 1, CAST(100 AS Decimal(18, 0)), CAST(100 AS Decimal(18, 0)), CAST(100 AS Decimal(18, 0)), N'', N'', CAST(N'2018-11-19T23:40:50.990' AS DateTime), 0, CAST(100 AS Decimal(18, 0)), CAST(100 AS Decimal(18, 0)), CAST(100 AS Decimal(18, 0)), N'2138213', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-11-19T23:46:05.260' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (15, CAST(N'2018-11-21T00:00:00.000' AS DateTime), CAST(N'2018-11-28T20:50:21.240' AS DateTime), 2, 2, 2, N'sadkaksd', 1, CAST(10 AS Decimal(18, 0)), CAST(100 AS Decimal(18, 0)), CAST(1 AS Decimal(18, 0)), N'', N'', CAST(N'2018-11-23T00:00:00.000' AS DateTime), 0, CAST(100 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), N'31231723123123613', CAST(10 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-11-21T22:06:28.587' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (17, CAST(N'2018-11-08T00:00:00.000' AS DateTime), CAST(N'2018-11-23T21:14:26.717' AS DateTime), 2, 1, 1, N'sdfkb', 2, CAST(2 AS Decimal(18, 0)), CAST(212 AS Decimal(18, 0)), CAST(314 AS Decimal(18, 0)), N'', N'', NULL, 0, CAST(100 AS Decimal(18, 0)), CAST(1 AS Decimal(18, 0)), CAST(2 AS Decimal(18, 0)), N'', CAST(1 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-11-23T21:10:55.660' AS DateTime), CAST(N'2018-11-14T00:00:00.000' AS DateTime), NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (18, CAST(N'2018-09-03T23:26:26.790' AS DateTime), CAST(N'2018-12-07T15:58:46.187' AS DateTime), 3, 0, 1, N'TEST', 9, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', CAST(N'2018-11-28T20:53:49.630' AS DateTime), 0, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), N'', CAST(200 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-11-28T20:54:06.127' AS DateTime), CAST(N'2018-11-28T20:53:49.630' AS DateTime), NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (19, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-11-28T23:35:05.870' AS DateTime), 3, 0, 1, N'TEST', 5, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', CAST(N'2018-11-28T21:02:00.203' AS DateTime), 0, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), N'', CAST(200 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-11-28T21:02:19.373' AS DateTime), CAST(N'2018-11-28T21:02:00.203' AS DateTime), NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (20, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-12-07T17:04:52.650' AS DateTime), 3, 0, 1, N'TEST', 1, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', CAST(N'2018-11-28T21:02:00.203' AS DateTime), 0, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), N'', CAST(200 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-11-28T23:35:07.323' AS DateTime), CAST(N'2018-11-28T21:02:00.203' AS DateTime), NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (24, CAST(N'2018-12-07T17:22:46.463' AS DateTime), CAST(N'2018-12-07T18:01:55.270' AS DateTime), 0, 0, 0, N'', 18, CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', NULL, 0, CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-12-07T17:23:01.713' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (28, CAST(N'2018-12-07T18:22:02.600' AS DateTime), CAST(N'2018-12-07T18:23:22.803' AS DateTime), 0, 0, 0, N'', 25, CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', NULL, 0, CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-12-07T18:22:22.213' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (29, CAST(N'2018-12-07T18:23:32.487' AS DateTime), CAST(N'2018-12-07T18:23:54.773' AS DateTime), 0, 0, 0, N'', 1, CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', NULL, 0, CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-12-07T18:23:37.963' AS DateTime), NULL, NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (30, CAST(N'2018-12-07T18:23:32.487' AS DateTime), CAST(N'2018-12-08T00:14:25.027' AS DateTime), 0, 0, 0, N'', 8, CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', CAST(N'2018-12-07T18:23:45.943' AS DateTime), 0, CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-12-07T18:23:56.760' AS DateTime), CAST(N'2018-12-07T18:23:45.943' AS DateTime), NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (31, CAST(N'2018-09-03T23:26:26.793' AS DateTime), CAST(N'2018-12-19T00:00:00.000' AS DateTime), 3, 6, 1, N'TEST', 24, CAST(5 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', CAST(N'2018-12-08T20:36:00.597' AS DateTime), 0, CAST(5000 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)), N'', CAST(200 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(N'2018-12-08T20:36:13.717' AS DateTime), CAST(N'2018-12-19T00:00:00.000' AS DateTime), NULL, N'Daily')
INSERT [dbo].[Arrivals] ([GuestId], [CheckInDate], [CheckoutDate], [NumberofGuests], [NumberofDays], [ModeofPaymentId], [GuestRemarks], [RoomId], [taxExemption], [RoomCharge], [RoomRate], [oldroom], [transferredroom], [expiredate], [CVV], [deposit], [CGST], [SGST], [cardnumber], [misc], [balance], [creation_date], [TentativeCheckout], [ServiceTax], [CheckinType]) VALUES (1031, CAST(N'2018-12-30T19:08:11.960' AS DateTime), NULL, 2, 2, 0, N'Retes', 13, CAST(0 AS Decimal(18, 0)), CAST(2060 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'', N'', NULL, 0, CAST(-4500 AS Decimal(18, 0)), CAST(100 AS Decimal(18, 0)), CAST(400 AS Decimal(18, 0)), N'', CAST(0 AS Decimal(18, 0)), CAST(1940 AS Decimal(18, 0)), CAST(N'2018-12-30T19:09:42.770' AS DateTime), CAST(N'2018-12-30T19:18:18.207' AS DateTime), CAST(10 AS Decimal(18, 0)), N'Daily')
SET IDENTITY_INSERT [dbo].[floors] ON 

INSERT [dbo].[floors] ([id], [floor_name]) VALUES (1, N'First Floor')
INSERT [dbo].[floors] ([id], [floor_name]) VALUES (2, N'Second Floor')
INSERT [dbo].[floors] ([id], [floor_name]) VALUES (3, N'Third Floor')
INSERT [dbo].[floors] ([id], [floor_name]) VALUES (4, N'Fourth Floor')
INSERT [dbo].[floors] ([id], [floor_name]) VALUES (5, N'Fifth Floor')
SET IDENTITY_INSERT [dbo].[floors] OFF
SET IDENTITY_INSERT [dbo].[Guests] ON 

INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (1, N'Raja Sekhar', CAST(N'1990-09-07T23:16:28.220' AS DateTime), N'Flat no 101, 
HIG 446, Hemachandra Apartments,
KPHB Colony 6th phase', N'1', N'3358 4062 2065', N'Maruti Alto 800', N'TS08FE6655', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (2, N'Santosh', CAST(N'1994-11-11T23:17:39.460' AS DateTime), N'Flat no 101, 
HIG 446, Hemachandra Apartments,
KPHB Colony 7th phase', N'1', N'3358 4062 1234', N'BMW X', N'AP31BH5730', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (3, N'Murthy', CAST(N'1985-10-29T23:20:34.787' AS DateTime), N'Flat no 202, 
HIG 446, Test Apartments,
KPHB Colony 1st phase', N'2', N'3358 1234 1234', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (4, N'Murali', CAST(N'1985-10-29T23:20:58.450' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'3', N'BWUPKKKK18', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (5, N'USER ID 5', CAST(N'1985-10-29T23:22:41.873' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'3', N'BWUPKKKK18', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (6, N'USER ID 6', CAST(N'1985-10-29T23:22:49.500' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'3', N'BWUPKKKK18', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (7, N'USER ID 7', CAST(N'1985-10-29T23:22:56.587' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'3', N'BWUPKKKK18', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (8, N'USER ID 8', CAST(N'1985-10-29T23:23:05.247' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'3', N'BWUPKKKK18', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (11, N'kbdhsdbhj', CAST(N'2019-01-01T00:00:00.000' AS DateTime), N'sdfksh', N'fsvgh\fsv', N'hbsjdvf', N'sdfvshdgv', N'df\svhgfxs', N'M', N'sdhfbsjgfb', N'hsdfbh\bs', N'fjsdbhgfb', N'873128389127', NULL, N'sdhgvfj\v', N'sfdgvj\gfv', N'asdfvshdgfv')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (12, N'askjbjadbsajh', CAST(N'2019-01-01T00:00:00.000' AS DateTime), N'sdjkfbsdb', N'hsdbhgfs', N'fjvsjdfj', N'fsdjfjs', N'jfxdsjvcjsdf', N'M', N'sdfjhbfs', N'dsfjbfj\sb', N'fsdhbsjfb', N'fsdhbfsj', NULL, N'jhfbjdsgvfsjv', N'fsdjvsjf', N'fjsdvjfs')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (13, N'qwhjbjd', CAST(N'2018-11-12T00:00:00.000' AS DateTime), N'adjb', N'sfdjdvsf', N'fsdjvf', N'fjdsvjgvf', N'fvdsjgvf', N'M', N'dsjgfb', N'sdhfb', N'sdjf', N'jsdfjv', NULL, N'jsdfvjsvf', N'djvgvfsjdvf', N'fjsdvf')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (14, N'Raja Sekhar', CAST(N'1990-11-21T00:00:00.000' AS DateTime), N'Test Address', N'ajfdhsdgf', N'sadjkasbh', N'fdgvs', N'dssdfjsfdvg', N'M', N'500072', N'Hyderabad', N'Telangana', N'9000450268', NULL, N'ghadvg', N'sdfv', N'fsdhgv')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (15, N'RajaSekhar', NULL, N'test', N'fbehjs', N'dfbjsb', N'sdfv', N'sfbhdv', N'M', N'adjjk', N'skjdnkdfb', N'hj\bdshf\s', N'8374020033', NULL, N'bdgh\svg', N'sdghv', N'fjsd')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (16, N'asdkbashbd', NULL, N'shbdjh\bdsf', N'fbjd\hbfsd', N'dsbhgfbs', N'fdshbjfsb', N'fsdhbjgfb', N'M', N'sdjbjgfs', N'fdj\sbjf', N'sdhbjgbs', N'fdhsbjgfb', NULL, N'fdsjbfjs', N'fdshbjgbfs', N'fbsdhb')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (17, N'askdbaj\bd', NULL, N'sdkbshjb', N'sdhbfj\bs', N'sdhbj\bf', N'fsdhbhg\f', N'fsdhbjgfb', N'M', N'sdbhfh\b', N'sdhbjb', N'fdshbj\gfs', N'dshbfj\bf', NULL, N'fdsbhgfb', N'fsdhbfds', N'sdbjg\sbd')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (18, N'Murali', CAST(N'1985-10-29T23:20:58.450' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'', N'BWUPKKKK18', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (19, N'USER ID 5', CAST(N'1985-10-29T23:22:41.873' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'', N'BWUPKKKK18', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (20, N'USER ID 5', CAST(N'1985-10-29T23:22:41.873' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'', N'BWUPKKKK18', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (21, N'aksjdbasd', NULL, N'sjkdfsbf', N'dsdf', N'dass', N'fsdjfs', N'dsdfs', N'M', N'dsfhbsjb', N'fdhsbjg\fs', N'sdhfbsjbf', N'asdads', NULL, N'sdfsf', N'dskfbj\bf', N'sdhfbshfb')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (22, N'hfggfxg', NULL, N'hjjhvjh', N'hvghv', N'gfdgf', N'hb', N'kjbhj', N'M', N'jkbk', N'hjhv', N'jhhjv', N'87898', NULL, N'', N'jhbjh', N'hbhj')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (23, N'asdkjasd', NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (24, N'etdahbsjbdjabs', NULL, N'akjbhjba', N'', N'', N'', N'', N'M', N'askjdbasd', N'dhbh\bsf', N'j\bsdhj\sb', N'fdshbf', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (25, N'kjasbdkbasd', NULL, N'sdkfbhsb', N'fdj\bhfs', N'fdhsbgsb', N'hfdbj\bsf', N'fsdjbhg\sb', N'M', N'skdfbsd', N'hbdshgfbj\bf', N'sdbhbf', N'sdhbjhbsdf', NULL, N'fbsjbdsfj', N'fsdjbfjs', N'fsdjbfs')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (26, N'asdsad', NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (27, N'askjda', NULL, N'', N'', N'', N'', N'', N'M', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (28, N'akjsbdjkabsd', NULL, N'basdh\bsd', N'hj\dbasjabsd', N'sahbdjab', N'hdbsajbd', N'ashbda', N'M', N'sahbdjba', N'hbasjbdahsbd', N'hjbasjbd', N'jbasjbdja', NULL, N'ajsbdja', N'ashbad', N'jhb')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (29, N'asdnkajsdk', NULL, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (30, N'asdnkajsdk', CAST(N'2018-12-07T18:23:45.943' AS DateTime), N'', N'', N'', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (31, N'USER ID 6', CAST(N'1985-10-29T23:22:49.500' AS DateTime), N'Flat no 202, 
HIG 123, Test Apartments,
KPHB Colony 1st phase', N'', N'BWUPKKKK18', N'', N'', N'', N'', N'', N'', N'', NULL, N'', N'', N'')
INSERT [dbo].[Guests] ([Id], [FullName], [DateOfBirth], [Address], [IdProofId], [IdProofNumber], [CarModel], [RTGNum], [gender], [zipcode], [city], [state], [phonenumber], [emailid], [MoreIds], [purposeofvisit], [company]) VALUES (1031, N'Raja Sekhar', NULL, N'Test', N'9812u', N'892', N'sdjfb', N'sjbdfhj\s', N'M', N'500072', N'Hyderabad', N'Andhra', N'81273', NULL, N'19028', N'hsdbjgsf', N'jbsdfhbjfs')
SET IDENTITY_INSERT [dbo].[Guests] OFF
SET IDENTITY_INSERT [dbo].[LookupProofs] ON 

INSERT [dbo].[LookupProofs] ([Id], [code], [description]) VALUES (1, N'Aadhar', N'Aadhar Card')
INSERT [dbo].[LookupProofs] ([Id], [code], [description]) VALUES (2, N'Pan', N'Pan Card')
INSERT [dbo].[LookupProofs] ([Id], [code], [description]) VALUES (3, N'License', N'Driving License')
INSERT [dbo].[LookupProofs] ([Id], [code], [description]) VALUES (4, N'Passport', N'Passport')
SET IDENTITY_INSERT [dbo].[LookupProofs] OFF
INSERT [dbo].[LookupRoomType] ([id], [description], [code], [RoomCharge], [CentralGST], [StateGST], [ServiceTax]) VALUES (1, N'Standard', N'STD', CAST(1000 AS Decimal(18, 0)), CAST(180 AS Decimal(18, 0)), CAST(180 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[LookupRoomType] ([id], [description], [code], [RoomCharge], [CentralGST], [StateGST], [ServiceTax]) VALUES (2, N'Deluxe', N'DLX', CAST(2000 AS Decimal(18, 0)), CAST(100 AS Decimal(18, 0)), CAST(400 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[LookupRoomType] ([id], [description], [code], [RoomCharge], [CentralGST], [StateGST], [ServiceTax]) VALUES (4, N'Executive', N'EXEC', CAST(3000 AS Decimal(18, 0)), CAST(500 AS Decimal(18, 0)), CAST(600 AS Decimal(18, 0)), CAST(20 AS Decimal(18, 0)))
INSERT [dbo].[LookupRoomType] ([id], [description], [code], [RoomCharge], [CentralGST], [StateGST], [ServiceTax]) VALUES (5, N'Supreme', N'SUPR', CAST(4000 AS Decimal(18, 0)), CAST(400 AS Decimal(18, 0)), CAST(500 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[LookupRoomType] ([id], [description], [code], [RoomCharge], [CentralGST], [StateGST], [ServiceTax]) VALUES (6, N'Queen', N'QUN', CAST(2500 AS Decimal(18, 0)), CAST(200 AS Decimal(18, 0)), CAST(200 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[LookupStatus] ON 

INSERT [dbo].[LookupStatus] ([Id], [name], [description]) VALUES (1, N'Rented', N'Rooms that are occupied')
INSERT [dbo].[LookupStatus] ([Id], [name], [description]) VALUES (2, N'Vacant', N'Rooms that are Vacant')
INSERT [dbo].[LookupStatus] ([Id], [name], [description]) VALUES (3, N'Repair', N'Rooms that are under Repair')
INSERT [dbo].[LookupStatus] ([Id], [name], [description]) VALUES (4, N'Dirty', N'Rooms that need to be cleaned')
SET IDENTITY_INSERT [dbo].[LookupStatus] OFF
SET IDENTITY_INSERT [dbo].[ModesofPayment] ON 

INSERT [dbo].[ModesofPayment] ([code], [description], [id]) VALUES (N'CASH', N'Cash', 1)
INSERT [dbo].[ModesofPayment] ([code], [description], [id]) VALUES (N'CREDITCARD', N'Credit Card', 2)
INSERT [dbo].[ModesofPayment] ([code], [description], [id]) VALUES (N'DEBITCARD', N'Debit Card', 3)
SET IDENTITY_INSERT [dbo].[ModesofPayment] OFF
SET IDENTITY_INSERT [dbo].[rooms] ON 

INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (1, 1, 101, N'101', 2, 1)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (2, 1, 102, N'102', 2, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (4, 1, 103, N'103', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (5, 1, 104, N'104', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (6, 1, 105, N'105', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (7, 1, 106, N'106', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (8, 2, 201, N'201', 2, 5)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (9, 2, 202, N'202', 2, 5)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (10, 2, 203, N'203', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (11, 2, 204, N'204', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (12, 2, 205, N'205', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (13, 2, 206, N'206', 1, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (14, 2, 207, N'207', 2, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (15, 3, 301, N'301', 2, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (16, 3, 302, N'302', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (17, 3, 303, N'303', 2, 1)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (18, 3, 304, N'304', 3, 1)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (19, 3, 305, N'305', 2, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (21, 3, 306, N'306', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (22, 4, 401, N'401', 2, 1)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (23, 4, 402, N'402', 2, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (24, 4, 403, N'403', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (25, 4, 404, N'404', 2, 1)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (26, 4, 405, N'405', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (27, 4, 406, N'406', 2, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (28, 5, 501, N'501', 2, 1)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (29, 5, 502, N'502', 2, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (30, 5, 503, N'503', 3, 6)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (31, 5, 504, N'504', 2, 1)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (32, 5, 505, N'505', 2, 4)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (33, 5, 506, N'506', 2, 2)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (34, 5, 507, N'507', 2, 5)
INSERT [dbo].[rooms] ([id], [floor_id], [room_no], [room_display], [status], [ROOM_TYPE]) VALUES (35, 5, 508, N'508', 2, 6)
SET IDENTITY_INSERT [dbo].[rooms] OFF
SET IDENTITY_INSERT [dbo].[shiftmanagements] ON 

INSERT [dbo].[shiftmanagements] ([ID], [UserId], [ShiftName], [ShiftStartTime], [ShiftEndTime]) VALUES (1, 1, N'Test', CAST(N'2018-11-01T00:00:00.000' AS DateTime), CAST(N'2018-11-06T00:00:00.000' AS DateTime))
INSERT [dbo].[shiftmanagements] ([ID], [UserId], [ShiftName], [ShiftStartTime], [ShiftEndTime]) VALUES (2, 6, N'Morning', CAST(N'2018-11-22T00:00:00.000' AS DateTime), CAST(N'2018-11-22T00:00:00.000' AS DateTime))
INSERT [dbo].[shiftmanagements] ([ID], [UserId], [ShiftName], [ShiftStartTime], [ShiftEndTime]) VALUES (3, 7, N'Evening', CAST(N'2018-11-01T00:00:00.000' AS DateTime), CAST(N'2018-11-07T00:00:00.000' AS DateTime))
INSERT [dbo].[shiftmanagements] ([ID], [UserId], [ShiftName], [ShiftStartTime], [ShiftEndTime]) VALUES (4, 8, N'Night', CAST(N'2018-11-14T00:00:00.000' AS DateTime), CAST(N'2018-11-15T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[shiftmanagements] OFF
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([id], [username], [password], [fullname], [RoleId]) VALUES (1, N'hotel1', N'tes', N'Raja Sekhar', 1)
INSERT [dbo].[users] ([id], [username], [password], [fullname], [RoleId]) VALUES (6, N'Sekhar', N'password', N'RajaSekhar', 2)
INSERT [dbo].[users] ([id], [username], [password], [fullname], [RoleId]) VALUES (7, N'Santosh', N'password', N'Santosh', 2)
INSERT [dbo].[users] ([id], [username], [password], [fullname], [RoleId]) VALUES (8, N'Murthy', N'Password', N'Murthy', 2)
SET IDENTITY_INSERT [dbo].[users] OFF
ALTER TABLE [dbo].[Arrivals]  WITH CHECK ADD FOREIGN KEY([GuestId])
REFERENCES [dbo].[Guests] ([Id])
GO
ALTER TABLE [dbo].[Arrivals]  WITH CHECK ADD FOREIGN KEY([RoomId])
REFERENCES [dbo].[rooms] ([id])
GO
ALTER TABLE [dbo].[rooms]  WITH CHECK ADD FOREIGN KEY([ROOM_TYPE])
REFERENCES [dbo].[LookupRoomType] ([id])
GO
USE [master]
GO
ALTER DATABASE [HMSystem] SET  READ_WRITE 
GO
