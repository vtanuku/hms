1. Make sure you have microsoft office in your machine
2. Make sure you have .net framework 4.6.1 in your machine
3. If you do not have .net framework please download from https://www.microsoft.com/en-in/download/details.aspx?id=49981
4. Please Download sql express and sql management studio from web
5. Once you finish installing step 4, please run "Databse Creation.sql" provided as part of setup
6. Once you are done with step 5, please run "Schema And Database Creation.sql" 
7. Then in the setup "HMSystem.rar" open file HMS.exe.config. 
8. Change the connection string <add name="HMS" connectionString="Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=HMSystem;Integrated Security=True;" /> to a proper one
9. Change the values for each below setup. 

<appSettings>
  <add key="SlideShowFolder" value="SlideShow" /> <!--Slide show folders-->
  <add key="ReportGenerationFolder" value="C:\Temp"/><!--Reports Generation Folder-->
  <add key="GuestPictureStoragePath" value="C:\Temp"/> 
  <add key="LocalIdStoragePath" value="C:\Temp"/>
  <add key="LogoImage" value="C:\HMS\hms\HMS\Resources\HotelImage1.jpg"/><!--Bill logo image-->
</appSettings>
