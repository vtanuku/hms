﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Utils
{
    public enum RoomStatusEnum
    {
        CheckIn,
        Vacant,
        Dirty,
        UnderMaintenance,
        Transfer
    }
}
