﻿using HMS.DAL.Reposotories;
using HMS.Model;
using HMS.ViewModel;
using HMS.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace HMS
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private HMSService _service;
        
        private LoginViewModel _loginViewModel;
        private Login _loginView;

        void App_Startup(object sender, StartupEventArgs e)
        {
            HMSBootStrap _bootStrap = new HMSBootStrap();
            _service = _bootStrap.HMSService;
            _loginViewModel = new LoginViewModel(_service);
            _loginView = new Login(_loginViewModel);
            _loginView.Show();
        }
    }
}
