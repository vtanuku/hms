﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HMS.ViewModel;
using MahApps.Metro.Controls;

namespace HMS.Views
{
    /// <summary>
    /// Interaction logic for RoomsDataWindow.xaml
    /// </summary>
    public partial class RoomsDataWindow : MetroWindow
    {
        private RoomsDataViewModel _roomDataViewModel;
        
        public RoomsDataWindow(RoomsDataViewModel _roomDataViewModel)
        {
            InitializeComponent();
            DataContext = _roomDataViewModel;
        }
    }
}
