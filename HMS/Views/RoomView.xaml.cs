﻿using HMS.DAL.Reposotories;
using HMS.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HMS.Views
{
    /// <summary>
    /// Interaction logic for RoomView.xaml
    /// </summary>
    public partial class RoomView : MetroWindow
    {
        private HMSService _hmsService;

        public RoomView(RoomViewModel roomViewModel,bool isTransfer = false)
        {
            InitializeComponent();
            if (isTransfer)
                roomViewModel.ButtonText = "Transfer";
            else
                roomViewModel.ButtonText = "Checkout";
            this.DataContext = roomViewModel;
        }
    }
}
