﻿using HMS.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HMS.Views
{
    /// <summary>
    /// Interaction logic for ShiftView.xaml
    /// </summary>
    /// 
    public interface IShiftView
    {
        SecureString Password { get; }
    }
    public partial class ShiftView : MetroWindow, IShiftView
    {
        public ShiftView(ShiftViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        private void LoginViewModel_ClosingRequest(object sender, EventArgs e)
        {
            this.Close();
        }

        public SecureString Password
        {
            get
            {
                return txtPassword.SecurePassword;
            }
        }
    }
}
