﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HMS.ViewModel;
using MahApps.Metro.Controls;

namespace HMS.Views.Sub_Menus
{
    /// <summary>
    /// Interaction logic for MonthlyReports.xaml
    /// </summary>
    public partial class MonthlyReports : MetroWindow
    {
        private MonthlyReportsViewModel _monthlyReportsViewModel;
        

        public MonthlyReports(MonthlyReportsViewModel _monthlyReportsViewModel)
        {
            InitializeComponent();
            DataContext = _monthlyReportsViewModel;
        }
    }
}
