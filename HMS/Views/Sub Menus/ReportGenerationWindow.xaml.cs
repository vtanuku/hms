﻿using HMS.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HMS.Views.SubMenus
{
    /// <summary>
    /// Interaction logic for ReportGenerationWindow.xaml
    /// </summary>
    public partial class ReportGenerationWindow : MetroWindow
    {
        public ReportGenerationWindow(ReportGenerationViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
            viewModel.View = this;
        }
    }
}
