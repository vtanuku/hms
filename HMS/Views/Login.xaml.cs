﻿using HMS.DAL.Reposotories;
using HMS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HMS.Views
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    /// 
    public interface ILogin
    {
        SecureString Password { get; }
    }
    public partial class Login : Window, ILogin
    {
        
        public Login(LoginViewModel loginViewModel)
        {
            InitializeComponent();
            this.DataContext = loginViewModel;
            loginViewModel.ClosingRequest += LoginViewModel_ClosingRequest;
        }

        private void LoginViewModel_ClosingRequest(object sender, EventArgs e)
        {
            this.Close();
        }

        public SecureString Password
        {
            get
            {
                return txtPassword.SecurePassword;
            }
        }

        private void w1_Loaded(object sender, RoutedEventArgs e)
        {
            var anim = new DoubleAnimation(0, 1, (Duration)TimeSpan.FromSeconds(1));
            this.BeginAnimation(UIElement.OpacityProperty, anim);

            Storyboard sbAnimateImage = new Storyboard();
            DoubleAnimation doubleAnimation;

            doubleAnimation = new DoubleAnimation(0, new Duration(new TimeSpan(0, 0, 0, 3, 0)));
            Storyboard.SetTarget(doubleAnimation, keyImage);
            doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
            Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath("(Canvas.Right)"));
            sbAnimateImage.Children.Add(doubleAnimation);

            doubleAnimation = new DoubleAnimation(-35, new Duration(new TimeSpan(0, 0, 0, 2, 0)));
            doubleAnimation.AutoReverse = true;
            doubleAnimation.RepeatBehavior = RepeatBehavior.Forever;
            Storyboard.SetTarget(doubleAnimation, keyImage);
            Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath("(Canvas.Left)"));
            sbAnimateImage.Children.Add(doubleAnimation);

            Storyboard sbAnimateUser = new Storyboard();
            DoubleAnimation doubleAnimationUser;

            doubleAnimationUser = new DoubleAnimation(0, new Duration(new TimeSpan(0, 0, 0, 3, 0)));
            Storyboard.SetTarget(doubleAnimationUser, userImage);
            doubleAnimationUser.RepeatBehavior = RepeatBehavior.Forever;
            Storyboard.SetTargetProperty(doubleAnimationUser, new PropertyPath("(Canvas.Right)"));
            sbAnimateUser.Children.Add(doubleAnimationUser);

            doubleAnimationUser = new DoubleAnimation(-35, new Duration(new TimeSpan(0, 0, 0, 2, 0)));
            doubleAnimationUser.AutoReverse = true;
            doubleAnimationUser.RepeatBehavior = RepeatBehavior.Forever;
            Storyboard.SetTarget(doubleAnimationUser, userImage);
            Storyboard.SetTargetProperty(doubleAnimationUser, new PropertyPath("(Canvas.Left)"));
            sbAnimateUser.Children.Add(doubleAnimationUser);
            sbAnimateImage.Begin();
            sbAnimateUser.Begin();
        }
        

        private void w1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Closing -= w1_Closing;
            e.Cancel = true;
            var anim = new DoubleAnimation(0, (Duration)TimeSpan.FromSeconds(1));
            anim.Completed += (s, _) => 
            {
                this.Close();
            };
            this.BeginAnimation(UIElement.OpacityProperty, anim);
        }
    }
}
