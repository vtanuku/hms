﻿using HMS.ViewModel;
using HMS.Views.Sub_Menus;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HMS.Views
{
    
    /// <summary>
    /// Interaction logic for HotelManagSystem.xaml
    /// </summary>
    public partial class HotelManagSystem : MetroWindow
    {
        public int ImageCount { get; }
        public string[] Images { get; }

        private DispatcherTimer timer;
        static int ctr = 0;
        string slideShow = ConfigurationManager.AppSettings["SlideShowFolder"];
        private HotelManagSystemViewModel _viewModel;

        public HotelManagSystem(HotelManagSystemViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel;
            viewModel.RequestClose += OnRequestClose;
            System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            myDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 100); // 100 Milliseconds  
            myDispatcherTimer.Tick += myDispatcherTimer_Tick;
            myDispatcherTimer.Start();
            string currentPath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            ImageCount = Directory.GetFiles(currentPath + "/" + slideShow).Count();
            Images = Directory.GetFiles(currentPath + "/" + slideShow);
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 2);
            timer.IsEnabled = true;
            timer.Tick += new EventHandler(timer_Tick);
            this.DataContext = viewModel;
        }

        private void OnRequestClose()
        {
            this.Close();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (ctr > ImageCount - 1)
            {
                ctr = 0;
            }
            PlaySlideShow();
        }

        void myDispatcherTimer_Tick(object sender, EventArgs e)
        {
            tbk_clock.Text = DateTime.Now.ToString("hh:mm:ss");
        }

 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //var anim = new DoubleAnimation(0,1, (Duration)TimeSpan.FromSeconds(1));
          
            //this.BeginAnimation(UIElement.OpacityProperty, anim);
            //PlaySlideShow();
           // _viewModel.PopulateGraph();
        }


        private void PlaySlideShow()
        {
            try
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                string filename = Images.ElementAt(ctr);
                image.UriSource = new Uri(filename);
                image.EndInit();
                slideShowImage.Source = image;
                slideShowImage.Stretch = Stretch.Uniform;
                ctr++;
            }
            catch (Exception ex)
            {

            }
        }
        
        private void FrontDesk_MouseDoubleClick(object sender, EventArgs e)
        {
            var anim = new DoubleAnimation(0, (Duration)TimeSpan.FromSeconds(1));
            anim.Completed += (s, _) =>
            {
                //RoomsLayout _frontDesk = new RoomsLayout();
                //_frontDesk.Show();
                this.Close();
            };
            this.BeginAnimation(UIElement.OpacityProperty, anim);
        }

        private void Exit_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
        }

        private void ChartSeries_GotFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
        //    _viewModel.PopulateGraph();
        }
    }
}
