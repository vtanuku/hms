﻿using HMS.DAL.Reposotories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace HMS.Views
{
    /// <summary>
    /// Interaction logic for ShowWindowPath.xaml
    /// </summary>
    public partial class ShowWindowPath : Window
    {
        private int _id;

        public ShowWindowPath(int id)
        {
            InitializeComponent();
            _id = id;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (_id == 1)
            {
                if (Directory.Exists(txtPath.Text))
                {
                    //System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    //config.AppSettings.Settings["LocalIdStoragePath"].Value = txtPath.Text;
                    //config.Save(ConfigurationSaveMode.Modified);
                    //ConfigurationManager.RefreshSection("appSettings");
                    XmlDocument xmlDoc = new XmlDocument();
                    string strKey = "SlideShowFolder";
                    xmlDoc.Load(AppDomain.CurrentDomain.BaseDirectory + "\\HMS.exe.config");
                    if (!ConfigKeyExists(strKey))
                    {
                    
                    }

                    XmlNode appSettingsNode = xmlDoc.SelectSingleNode("configuration/appSettings");



                    foreach (XmlNode childNode in appSettingsNode)

                    {

                        if (childNode.Attributes["key"].Value == strKey)

                            childNode.Attributes["value"].Value = txtPath.Text;

                    }

                    xmlDoc.Save(AppDomain.CurrentDomain.BaseDirectory + "\\HMS.exe.config");

                    xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                    ConfigurationManager.RefreshSection("appSettings");
                }
                else
                {
                    MessageBox.Show("Directory doesn't exist");
                }
            }
            else if (_id == 2)
            {
                if (Directory.Exists(txtPath.Text))
                {
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["GuestPictureStoragePath"].Value = txtPath.Text;
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");
                }
                else
                {
                    MessageBox.Show("Directory doesn't exist");
                }
            }
            else if (_id == 3)
            {
                if (Directory.Exists(txtPath.Text))
                {
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["SlideShowFolder"].Value = txtPath.Text;
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");
                }
                else
                {
                    MessageBox.Show("Directory doesn't exist");
                }
            }
            else if (_id == 4)
            {
                if (Directory.Exists(txtPath.Text))
                {
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["ReportGenerationFolder"].Value = txtPath.Text;
                    config.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");
                }
                else
                {
                    MessageBox.Show("Directory doesn't exist");
                }
            }
            this.Close();
        }

        public bool ConfigKeyExists(string strKey)

        {

            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(AppDomain.CurrentDomain.BaseDirectory + "\\HMS.exe.config");



            XmlNode appSettingsNode = xmlDoc.SelectSingleNode("configuration/appSettings");



            foreach (XmlNode childNode in appSettingsNode)

            {

                if (childNode.Attributes["key"].Value == strKey)

                    return true;

            }

            return false;

        }
    }
}
