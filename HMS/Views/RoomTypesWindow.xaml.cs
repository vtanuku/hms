﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HMS.ViewModel;
using MahApps.Metro.Controls;

namespace HMS.Views
{
    /// <summary>
    /// Interaction logic for RoomTypesWindow.xaml
    /// </summary>
    public partial class RoomTypesWindow : MetroWindow
    {
        public RoomTypesWindow(RoomTypesWindowViewModel _roomTypeDetails)
        {
            InitializeComponent();
            this.DataContext = _roomTypeDetails;
        }
    }
}
