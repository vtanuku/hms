﻿using HMS.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HMS.Views
{
    /// <summary>
    /// Interaction logic for RoomsLayout.xaml
    /// </summary>
    public partial class RoomsLayout : MetroWindow
    {
        public RoomsLayout(RoomsLayoutViewModel roomsLayoutViewModel)
        {
            InitializeComponent();
            this.DataContext = roomsLayoutViewModel;
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {   
            var anim = new DoubleAnimation(0, (Duration)TimeSpan.FromSeconds(1));
            anim.Completed += (s, _) =>
            {
                //HotelManagSystem _hms = new HotelManagSystem();
                //_hms.Show();
                this.Close();
            };
            this.BeginAnimation(UIElement.OpacityProperty, anim);
        }

        //private void Rented_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        //{
        //    if(txt_RoomsRented.Value <0)
        //    {
        //        txt_RoomsRented.Value = 0;
        //    }
        //}
        //private void Vacant_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        //{
        //    if (txt_RoomsVacant.Value < 0)
        //    {
        //        txt_RoomsVacant.Value = 0;
        //    }

        //}
        //private void Dirty_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        //{
        //    if (txt_RoomsDirty.Value < 0)
        //    {
        //        txt_RoomsDirty.Value = 0;
        //    }

        //}
        //private void Repair_RoomsChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        //{
        //    if (txt_RoomsUnderRepair.Value < 0)
        //    {
        //        txt_RoomsUnderRepair.Value = 0;
        //    }

        //}
        //private void AllRooms_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        //{
        //    if (txt_AllRooms.Value < 0)
        //    {
        //        txt_AllRooms.Value = 0;
        //    }

        //}
    }
}
