﻿using HMS.DAL.Reposotories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS
{
    public class HMSBootStrap
    {
        private HMSService _hmsService;

        public HMSService HMSService
        {
            get
            {
                return _hmsService;
            }
        }
        public HMSBootStrap()
        {
            _hmsService = new HMSService();
        }
    }
}
