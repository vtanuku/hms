﻿using HMS.Model.Lookups;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HMS.ViewModel
{
    public class RoomTypeDetailViewModel : INotifyPropertyChanged
    {
        private RoomType _roomType;
        
        public event PropertyChangedEventHandler PropertyChanged;
        public RoomType RoomType
        {
            get
            {
                return _roomType;
            }
        }
        public RoomTypeDetailViewModel(RoomType roomType)
        {
            _roomType = roomType;
            IsDirty = false;
        }

        public int Id
        {
            get
            {
                return _roomType.Id;
            }
            set
            {
                _roomType.Id = value;
                OnPropertyChanged();
            }
        }

        public string RoomCode
        {
            get
            {
                return _roomType.Code;
            }
            set
            {
                _roomType.Code = value;
                OnPropertyChanged();
            }
        }

        public string RoomDescription
        {
            get
            {
                return _roomType.Description;
            }
            set
            {
                _roomType.Description = value;
                OnPropertyChanged();
            }
        }

        public decimal RoomCharge
        {
            get
            {
                return _roomType.RoomCharge;
            }
            set
            {
                _roomType.RoomCharge = value;
                OnPropertyChanged();
            }
        }
        public decimal CentralGST
        {
            get
            {
                return _roomType.CentralGST;
            }
            set
            {
                _roomType.CentralGST = value;
                OnPropertyChanged();
            }
        }
        public decimal StateGST
        {
            get
            {
                return _roomType.StateGST;
            }
            set
            {
                _roomType.StateGST = value;
                OnPropertyChanged();
            }
        }

        public decimal? RoomServiceTax
        {
            get
            {
                return _roomType.RoomServiceTax;
            }
            set
            {
                _roomType.RoomServiceTax = value;
                OnPropertyChanged();
            }
        }

        public bool IsDirty { get; set; }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
            IsDirty = true;
        }
    }
}