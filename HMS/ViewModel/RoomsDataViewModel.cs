﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HMS.ViewModel
{
    public class RoomsDataViewModel : INotifyPropertyChanged
    {
        private HMSService _hmsService;
        private RoomDetailsSingleViewModel _selectedRoom;

        public event PropertyChangedEventHandler PropertyChanged;

        public DelegateCommand AddRoomCommand { get; private set; }
        public DelegateCommand DeleteRoomCommand { get; private set; }
        public DelegateCommand SaveCommand { get; private set; }
        public ObservableCollection<RoomDetailsSingleViewModel> RoomDetails { get; set; }
        public RoomDetailsSingleViewModel SelectedRoom
        {
            get
            {
                return _selectedRoom;
            }
            set
            {
                _selectedRoom = value;
                OnPropertyChanged();
            }
        }

        public RoomsDataViewModel(HMSService hmsService)
        {
            _hmsService = hmsService;
            RoomDetails = new ObservableCollection<RoomDetailsSingleViewModel>();
            AddRoomCommand = new DelegateCommand(AddRoom, CanAddRoom);
            DeleteRoomCommand = new DelegateCommand(DeleteRoom, CanDeleteRoom);
            SaveCommand = new DelegateCommand(Save, CanSaveRoom);
            _hmsService.GetRooms().ToList().ForEach(r => RoomDetails.Add(new RoomDetailsSingleViewModel(r, _hmsService)));
        }

        private bool CanSaveRoom(object parameter)
        {
            return true;
        }

        private void Save(object parameter)
        {
            foreach (var item in RoomDetails.Where(r=>r.IsDirty))
            {
                if(item.Id ==0)
                {
                    _hmsService.InsertRoom(item.Room);
                }
                else
                {
                    _hmsService.UpdateRoom(item.Room);
                }
            }
            MessageBox.Show("Rooms updated Successfully");
        }

        private bool CanDeleteRoom(object parameter)
        {
            return true;
        }

        private void DeleteRoom(object parameter)
        {
            RoomDetails.Remove(SelectedRoom);            
        }

        private bool CanAddRoom(object parameter)
        {
            return true;
        }

        private void AddRoom(object parameter)
        {
            RoomDetails.Add(new RoomDetailsSingleViewModel(new Model.Rooms(), _hmsService));
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
