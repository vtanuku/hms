﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace HMS.ViewModel
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private HMSService _service;
        private string _displayName;
        private string _address;
        private string _userName;
        private string _passWord;

        public DelegateCommand LoginCommand { get; set; }

        public event EventHandler ClosingRequest;

        private void CloseWindow(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }
        public LoginViewModel(HMSService service)
        {
            _service = service;
            var viewModel = _service.GetHotelDetails().FirstOrDefault();
            DisplayName = viewModel.DisplayName;
            LoginCommand = new DelegateCommand(ShowHMSwindow, CanLoginWindow);
            Address = viewModel.Address;
        }

        private bool CanLoginWindow(object parameter)
        {
            return true;            
        }

        private void ShowHMSwindow(object parameter)
        {
            var users = _service.GetUsersList();
            var passwordContainer = parameter as ILogin;
            if (passwordContainer != null)
            {
                var secureString = passwordContainer.Password;
                Password = ConvertToUnsecureString(secureString);
            }
            string retVal = _service.CheckUserAccess(UserName, Password);

            if (retVal == "Success")
            {
                var user = users.FirstOrDefault(u => u.UserName == UserName);
                 _hotelManagementViewModel = new HotelManagSystemViewModel(_service, user);
                View = new HotelManagSystem(_hotelManagementViewModel);
                ClosingRequest?.Invoke(null, EventArgs.Empty);
                View.Show();
            }
            else
                MessageBox.Show(retVal);
        }

        private string ConvertToUnsecureString(SecureString securePassword)
        {
            if (securePassword == null)
            {
                return string.Empty;
            }

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        public string DisplayName
        {
            get
            {
                return _displayName;
            }

            set
            {
                _displayName = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get
            {
                return _address;
            }

            set
            {
                _address = value;
                OnPropertyChanged();
            }
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get
            {
                return _passWord;
            }
            set
            {
                _passWord = value;
                
            }
        }

        private HotelManagSystemViewModel _hotelManagementViewModel;

        public HotelManagSystem View { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
