﻿using HMS.DAL.Reposotories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMS.Model;
using HMS.Model.Lookups;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using HMS.Core.Command;

namespace HMS.ViewModel
{
    public class RoomMaintenanceViewModel:INotifyPropertyChanged
    {
        private HMSService _hmsService;
        private List<Rooms> _rooms;
        private List<Status> _statuses;
        private Rooms _selectedRoom;
        private Status _selectedStatus;

        public RoomMaintenanceViewModel(HMSService hmsService)
        {
            _hmsService = hmsService;
            _rooms = _hmsService.GetRooms().Where(r=>r.StatusId != 1).ToList();
            _statuses = _hmsService.GetStatuses();
            UpdateStatusCommand = new DelegateCommand(UpdateStatusOfRoom, CanUpdateStatus);
        }

        private bool CanUpdateStatus(object parameter)
        {
            return true;
        }

        private void UpdateStatusOfRoom(object parameter)
        {
            _hmsService.UpdateStatusOfRoom(SelectedRoom.Id, SelectedStatus.Id);
        }

        public List<Status> Statuses
        {
            get
            {
                return _statuses;
            }
            set
            {
                _statuses = value;
                OnPropertyChanged();
            }
            
        }

        public List<Rooms> Rooms
        {
            get
            {
                return _rooms;
            }
            set
            {
                _rooms = value;
                OnPropertyChanged();
            }
        }

        public Rooms SelectedRoom
        {
            get
            {
                return _selectedRoom;
            }
            set
            {
                _selectedRoom = value;
                OnPropertyChanged();
            }
        }

        public Status SelectedStatus
        {
            get
            {
                return _selectedStatus;
            }
            set
            {
                _selectedStatus = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand UpdateStatusCommand { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
