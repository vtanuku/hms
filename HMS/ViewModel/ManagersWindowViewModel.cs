﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Views;
using HMS.Views.Sub_Menus;
using HMS.Views.SubMenus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.ViewModel
{
    public class ManagersWindowViewModel
    {
        private HMSService _hmsService;
        private ReportsWindowViewModel _reportsGenerationWindowViewModel;
        private ReportsWindow reportsGenerationView;
        private ShiftViewModel _shiftViewModel;
        private ShiftView _shiftsView;
        private ManageShifts _manageShifts;
        private ManageShiftsViewModel _mgmtShiftsViewModel;
        private SettingsWindowViewModel _settingsWindowViewModel;
        private SettingsWindow _settingsWindow;

        public DelegateCommand ShowReportsWindowCommand { get; set; }
        public DelegateCommand CloseWindowCommand { get; set; }
        public ManagersMenu View { get; set; }
        public DelegateCommand ShowAddUserCommand { get; private set; }
        public DelegateCommand ShiftManagementCommand { get; private set; }
        public DelegateCommand SettingsWindowCommand { get; private set; }

        public ManagersWindowViewModel(HMSService hmsService)
        {
            _hmsService = hmsService;
            ShowReportsWindowCommand = new DelegateCommand(ShowReportsWindow, CanShowReportsWindow);
            CloseWindowCommand = new DelegateCommand(CloseWindow, CanCloseWindow);
            ShowAddUserCommand = new DelegateCommand(ShowAddUser, CanShowAddUser);
            ShiftManagementCommand = new DelegateCommand(ManageShiftsOfUsers, CanManageShift);
            SettingsWindowCommand = new DelegateCommand(ManageSettings, CanManageSettings);
        }

        private void ManageSettings(object parameter)
        {
            _settingsWindowViewModel = new SettingsWindowViewModel(_hmsService);
            _settingsWindow = new SettingsWindow(_settingsWindowViewModel);
            _settingsWindow.ShowDialog();
        }

        private bool CanManageSettings(object parameter)
        {
            return true;
        }

        private bool CanManageShift(object parameter)
        {
            return true;
        }

        private void ManageShiftsOfUsers(object parameter)
        {
            _mgmtShiftsViewModel = new ManageShiftsViewModel(_hmsService);
            _manageShifts = new ManageShifts(_mgmtShiftsViewModel);
            _manageShifts.ShowDialog();
        }

        private bool CanShowAddUser(object parameter)
        {
            return true;
        }

        private void ShowAddUser(object parameter)
        {
            _shiftViewModel = new ShiftViewModel(_hmsService);
            _shiftsView = new ShiftView(_shiftViewModel);
            _shiftsView.ShowDialog();
        }
        private void CloseWindow(object parameter)
        {
            View?.Close();
        }

        private bool CanCloseWindow(object parameter)
        {
            return true;
        }

        private void ShowReportsWindow(object parameter)
        {
            _reportsGenerationWindowViewModel = new ReportsWindowViewModel(_hmsService);
            reportsGenerationView = new ReportsWindow(_reportsGenerationWindowViewModel);
            reportsGenerationView.ShowDialog();
        }

        private bool CanShowReportsWindow(object parameter)
        {
            return true;
        }
    }
}
