﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace HMS.ViewModel
{
    public class ShiftViewModel : INotifyPropertyChanged
    {
        private HMSService _hmsService;
        private string _userName;
        private string _fullName;

        private string _role;
        private List<string> _roles;
        private string _passWord;

        public ShiftViewModel(HMSService hmsService)
        {
            _hmsService = new HMSService();
            _roles = new List<string>() { "Manager", "User" };
            AddUserCommand = new DelegateCommand(AddUser, CanAddUser);
        }

        private void AddUser(object parameter)
        {
            var passwordContainer = parameter as IShiftView;
            if (passwordContainer != null)
            {
                var secureString = passwordContainer.Password;
                Password = ConvertToUnsecureString(secureString);
            }
            _hmsService.AddUser(UserName, Password, FullName, RoleId);
        }

        private string ConvertToUnsecureString(SecureString securePassword)
        {
            if (securePassword == null)
            {
                return string.Empty;
            }

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        private bool CanAddUser(object parameter)
        {
            return true;
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
                OnPropertyChanged();
            }
        }

        public string FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get
            {
                return _passWord;
            }
            set
            {
                _passWord = value;

            }
        }

        public string Role
        {
            get
            {

                return _role;
            }
            set
            {
                _role = value;
                if(value == "Manager")
                {
                    RoleId = 1;
                }
                else
                {
                    RoleId = 2;
                }
                OnPropertyChanged();
            }
        }

        public List<string> Roles
        {
            get
            {
                return _roles;
            }
            set
            {
                _roles = value;
                OnPropertyChanged();
            }
        }


        public int RoleId
        {
            get;set;
        }
        public DelegateCommand AddUserCommand { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void AddUser()
        {

        }
    }
}
