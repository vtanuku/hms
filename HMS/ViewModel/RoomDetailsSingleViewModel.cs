﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Model;
using HMS.Model.Lookups;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HMS.ViewModel
{
    public class RoomDetailsSingleViewModel : INotifyPropertyChanged
    {
        private HMSService _hmsService;
        private Rooms _room;

        public Rooms Room
        {
            get
            {
                return _room;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public RoomDetailsSingleViewModel(Rooms room, HMSService hmsService)
        {
            _room = room;
            _hmsService = hmsService;
            RoomTypes = new ObservableCollection<RoomType>(_hmsService.GetRoomTypes());
            SelectedRoomType = RoomTypes.FirstOrDefault(r => r.Id == room.RoomType?.Id);
            IsDirty = false;        
        }

        public int Id
        {
            get
            {
                return _room.Id;
            }
            set
            {
                _room.Id = value;
                OnPropertyChanged();
            }
        }
        public int FloorNumber
        {
            get
            {
                return _room.FloorId;
            }
            set
            {
                _room.FloorId = value;
                OnPropertyChanged();
            }
        }
        public string RoomNumber
        {
            get
            {
                return _room.RoomNumber;
            }
            set
            {
                _room.RoomNumber = value;
                OnPropertyChanged();
            }
        }

        public string RoomDisplay
        {
            get
            {
                return _room.Room_Display;
            }
            set
            {
                _room.Room_Display = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<RoomType> RoomTypes { get; set; }
        public RoomType SelectedRoomType
        {
            get
            {
                return _room.RoomType;
            }
            set
            {
                _room.RoomType = value;
                OnPropertyChanged();
            }
        }

        public bool IsDirty { get; set; }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
            IsDirty = true;
        }
    }
}
