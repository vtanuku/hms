﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HMS.ViewModel
{
    public class MonthlyReportsViewModel : INotifyPropertyChanged
    {
        private HMSService _hmsService;
        private int _selectedStartYear;
        private int _selectedEndYear;
        private string _selectedStartMonth;
        private string _selectedEndMonth;
        private int _selectedStartIndex;
        private int _selectedEndIndex;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<int> Years { get; set; }
        public ObservableCollection<string> Months { get; set; }
        public MonthlyReportsViewModel(HMSService hmsService)
        {
            _hmsService = hmsService;
            Months = new ObservableCollection<string>();
            Years = new ObservableCollection<int>();
            GenerateMonthlyReportCommand = new DelegateCommand(GenerateReport, CanGenerateReport);
            for(int i = DateTime.Now.Year; i>=1950;i--)
            {
                Years.Add(i);
            }
            Months.Add("January");
            Months.Add("February");
            Months.Add("March");
            Months.Add("April");
            Months.Add("May");
            Months.Add("June");
            Months.Add("July");
            Months.Add("August");
            Months.Add("September");
            Months.Add("October");
            Months.Add("November");
            Months.Add("December");
        }

        public int SelectedStartYear
        {
            get
            {
                return _selectedStartYear;
            }
            set
            {
                _selectedStartYear = value;
                OnPropertyChanged();
            }
        }

        public int SelectedEndYear
        {
            get
            {
                return _selectedEndYear;
            }
            set
            {
                _selectedEndYear = value;
                OnPropertyChanged();
            }
        }

        public string SelectedStartMonth
        {
            get
            {
                return _selectedStartMonth;
            }
            set
            {
                _selectedStartMonth = value;
                OnPropertyChanged();
            }
        }
        public int SelectedStartIndex
        {
            get
            {
                return _selectedStartIndex;
            }
            set
            {
                _selectedStartIndex = value;
                OnPropertyChanged();
            }
        }

        public int SelectedEndIndex
        {
            get
            {
                return _selectedEndIndex;
            }
            set
            {
                _selectedEndIndex = value;
                OnPropertyChanged();
            }
        }

        public string SelectedEndMonth
        {
            get
            {
                return _selectedEndMonth;
            }
            set
            {
                _selectedEndMonth = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand GenerateMonthlyReportCommand { get; private set; }

        private bool CanGenerateReport(object parameter)
        {
            return true;
        }

        private void GenerateReport(object parameter)
        {
            DateTime startDate = new DateTime(SelectedStartYear, SelectedStartIndex+1, 1);
            DateTime endDate = new DateTime(SelectedEndYear, SelectedEndIndex+1, DateTime.DaysInMonth(SelectedEndYear, SelectedEndIndex+1));
            _hmsService.GetMonthlyTotalsReport(startDate, endDate, Core.Enums.ReportEnums.EnumReportType.MonthlyTotals);
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
