﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.ViewModel
{
    public class SettingsWindowViewModel
    {
        private HMSService _hmsService;
        private RoomTypesWindowViewModel _roomTypeDetails;
        private RoomTypesWindow _roomTypesWindow;
        private RoomsDataViewModel _roomDataViewModel;
        private RoomsDataWindow _roomDataView;
        private ShowWindowPath _view;

        public SettingsWindowViewModel(HMSService hmsService)
        {
            _hmsService = hmsService;
            AddEditRoomTypesCommand = new DelegateCommand(AddEditRoomTypes, CanAddEditRoomTypes);
            ShowRoomDetailsCommand = new DelegateCommand(ShowRoomDetails, CanShowRoomDetails);
            ConfigureLocalIdStorageCommand = new DelegateCommand(ConfigureLocalId, CanConfigureLocalId);
            ConfigureGuestWindowCommand = new DelegateCommand(ConfigureGuestWindow, CanConfigureLocalId);
            ConfigureSlideShowCommand = new DelegateCommand(ConfigureSlideShow, CanConfigureLocalId);
            ConfigureReportPathCommand = new DelegateCommand(ConfigureReportPath, CanConfigureLocalId);
        }

        private void ConfigureReportPath(object parameter)
        {
            _view = new ShowWindowPath(4);
            _view.ShowDialog();
        }

        private void ConfigureSlideShow(object parameter)
        {
            _view = new ShowWindowPath(3);
            _view.ShowDialog();
        }

        private void ConfigureGuestWindow(object parameter)
        {
            _view = new ShowWindowPath(2);
            _view.ShowDialog();
        }

        private void ConfigureLocalId(object parameter)
        {
            _view = new ShowWindowPath(1);
            _view.ShowDialog();
        }

        private bool CanConfigureLocalId(object parameter)
        {
            return true;
        }

        private void ShowRoomDetails(object parameter)
        {
            _roomDataViewModel = new RoomsDataViewModel(_hmsService);
            _roomDataView = new RoomsDataWindow(_roomDataViewModel);
            _roomDataView.ShowDialog();
        }

        private bool CanShowRoomDetails(object parameter)
        {
            return true;
        }

        private bool CanAddEditRoomTypes(object parameter)
        {
            return true;
        }

        private void AddEditRoomTypes(object parameter)
        {
            _roomTypeDetails = new RoomTypesWindowViewModel(_hmsService);
            _roomTypesWindow = new RoomTypesWindow(_roomTypeDetails);
            _roomTypesWindow.ShowDialog();
        }

        public DelegateCommand AddEditRoomTypesCommand { get; private set; }
        public DelegateCommand ShowRoomDetailsCommand { get; private set; }
        public DelegateCommand ConfigureLocalIdStorageCommand { get; private set; }
        public DelegateCommand ConfigureGuestWindowCommand { get; private set; }
        public DelegateCommand ConfigureSlideShowCommand { get; private set; }
        public DelegateCommand ConfigureReportPathCommand { get; private set; }
    }
}
