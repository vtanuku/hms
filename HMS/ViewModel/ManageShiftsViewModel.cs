﻿using HMS.Core.Command;
using HMS.DAL.DAL.Model;
using HMS.DAL.Reposotories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.ViewModel
{
    public class ManageShiftsViewModel
    {
        private HMSService _hmsService;

        public ManageShiftsViewModel(HMSService hmsService)
        {
            _hmsService = hmsService;
            ManageShifts = new ObservableCollection<ShiftManagement>(_hmsService.GetShiftManagements());
            UpdateAllShiftsCommand = new DelegateCommand(UpdateCommands, CanUpdateCommand);
        }

        private bool CanUpdateCommand(object parameter)
        {
            return true;
        }

        private void UpdateCommands(object parameter)
        {
            foreach (var item in ManageShifts)
            {
                _hmsService.UpdateAllUserShifts(item.UserId, item.ShiftName, item.ShiftStartTime, item.ShiftEndTime);
            }
        }

        public ObservableCollection<ShiftManagement> ManageShifts { get; set; }
        public DelegateCommand UpdateAllShiftsCommand { get; private set; }
    }
}
