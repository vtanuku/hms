﻿using HMS.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HMS.ViewModel
{
    public class RoomsOccupancyViewModel : INotifyPropertyChanged
    {
        private int _id;
        private string _roomName;
        private string _floorName;
        private string _roomType;
        private string _guestName;
        private DateTime? _checkInDate;
        private DateTime? _checkOutDate;
        private DateTime? _tentativeCheckoutDate;
        private int _numberOfHoursLeft;
        private int _statusId;

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }
        public string RoomName
        {
            get
            {
                return _roomName;
            }
            set
            {
                _roomName = value;
                OnPropertyChanged();
            }
        }
        public string FloorName
        {
            get
            {
                return _floorName;
            }
            set
            {
                _floorName = value;
                OnPropertyChanged();
            }
        }
        public string RoomType
        {
            get
            {
                return _roomType;
            }
            set
            {
                _roomType = value;
                OnPropertyChanged();
            }
        }

        public string GuestName
        {
            get
            {
                return _guestName;
            }
            set
            {
                _guestName = value;
                OnPropertyChanged();
            }
        }
        public DateTime? CheckInDate
        {
            get
            {
                return _checkInDate;
            }
            set
            {
                _checkInDate = value;
                OnPropertyChanged();
            }
        }
        public DateTime? CheckOutDate
        {
            get
            {
                return _checkOutDate;
            }
            set
            {
                _checkOutDate = value;
                OnPropertyChanged();
            }
        }
        public DateTime? TentativeCheckOutDate
        {
            get
            {
                return _tentativeCheckoutDate;
            }
            set
            {
                _tentativeCheckoutDate = value;
                OnPropertyChanged();
            }
        }
        public int NumberofHoursLeft
        {
            get
            {
                return _numberOfHoursLeft;
            }
            set
            {
                _numberOfHoursLeft = value;
                OnPropertyChanged();
            }
        }
        public int StatusId
        {
            get
            {
                return _statusId;
            }
            set
            {
                _statusId = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}