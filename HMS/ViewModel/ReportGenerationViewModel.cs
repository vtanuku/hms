﻿using HMS.Core.Command;
using HMS.Core.Enums;
using HMS.DAL.Reposotories;
using HMS.Views.SubMenus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static HMS.Core.Enums.ReportEnums;

namespace HMS.ViewModel
{
    public class ReportGenerationViewModel : INotifyPropertyChanged
    {
        private HMSService _hmsService;
        public ReportGenerationWindow View;
        private EnumReportType _reportToBeGenerated;
        private DateTime _selectedStartDate;
        private DateTime _selectedEndDate;
        private string _roomNumber;
        private Visibility _roomNumberVisible;

        public event PropertyChangedEventHandler PropertyChanged;

        public DelegateCommand GenerateReportCommand { get; set; }
        public DelegateCommand DoNotGenerateReportCommand { get; private set; }

        public ReportGenerationViewModel(HMSService hmsService, EnumReportType reportTobeGenerated, bool roomNumberVisible = false)
        {
            _hmsService = hmsService;
            GenerateReportCommand = new DelegateCommand(GenerateReport, CanGenerateReport);
            DoNotGenerateReportCommand = new DelegateCommand(CloseWindow, CanCloseWindow);
            _reportToBeGenerated = reportTobeGenerated;
            if (roomNumberVisible)
                RoomNumberVisible = Visibility.Visible;
            else
                RoomNumberVisible = Visibility.Collapsed;
        }

        private bool CanCloseWindow(object parameter)
        {
            return true;
        }

        private void CloseWindow(object parameter)
        {
            View?.Close();
        }

        private bool CanGenerateReport(object parameter)
        {
            return true;
        }
        public DateTime SelectedStartDate
        {
            get { return _selectedStartDate; }
            set { _selectedStartDate = value;OnPropertyChanged(); }
        }

        public string RoomNumber
        {
            get { return _roomNumber; }
            set { _roomNumber = value; OnPropertyChanged(); }
        }
        public DateTime SelectedEndDate
        {
            get { return _selectedEndDate; }
            set { _selectedEndDate = value; OnPropertyChanged(); }
        }

        public Visibility RoomNumberVisible
        {
            get { return _roomNumberVisible; }
            set { _roomNumberVisible = value; OnPropertyChanged(); }
        }


        private void GenerateReport(object parameter)
        {
            switch (_reportToBeGenerated)
            {
                case EnumReportType.DailyRoomAndTax:
                    _hmsService.GetDailyReportsforRoomAndTax(SelectedStartDate, SelectedEndDate, EnumReportType.DailyRoomAndTax);
                    View.Close();
                    break;

                case EnumReportType.DailyAuditReport:
                    _hmsService.GetDalilyReportsForAudit(SelectedStartDate, SelectedEndDate, EnumReportType.DailyAuditReport);
                    View.Close();
                    break;

                case EnumReportType.IncomeByDateRange:
                    _hmsService.GetDateRangeIncomeDateRange(SelectedStartDate, SelectedEndDate, EnumReportType.IncomeByDateRange);
                    
                    break;
                case EnumReportType.RoomTaxDateRange:
                    _hmsService.GetDailyReportsforRoomAndTax(SelectedStartDate, SelectedEndDate, EnumReportType.RoomTaxDateRange);
                    break;
                case EnumReportType.TaxExemptionsReport:
                    _hmsService.GetTaxExemptionReport(SelectedStartDate, SelectedEndDate,EnumReportType.TaxExemptionsReport);
                    break;
                case EnumReportType.RoomRevenuebyDateRange:
                    _hmsService.GetRoomRevenueByDateRange(SelectedStartDate, SelectedEndDate, EnumReportType.RoomRevenuebyDateRange, RoomNumber);
                    break;
                case EnumReportType.MonthlyTotals:
                    _hmsService.GetMonthlyTotalsByDateRange(SelectedStartDate, SelectedEndDate, EnumReportType.MonthlyTotals);
                    break;

            }
            View.Close();
            
        }
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
