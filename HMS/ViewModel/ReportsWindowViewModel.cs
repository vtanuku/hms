﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMS.DAL.Reposotories;
using HMS.Core.Command;
using HMS.Views.Sub_Menus;
using HMS.Core.Enums;
using HMS.Views.SubMenus;

namespace HMS.ViewModel
{
    public class ReportsWindowViewModel
    {
        private HMSService _service;

        public ReportsWindow View;
        private ReportGenerationViewModel _reportGenerationViewModel;
        private ReportGenerationWindow _reportGenerationView;
        private MonthlyReportsViewModel _monthlyReportsViewModel;
        private MonthlyReports _monthlyReportsView;

        public DelegateCommand DailyRoomCollectedCommand { get; private set; }
        public DelegateCommand DailyAuditReportCommand { get; private set; }
        public DelegateCommand IncomeTaxByDateRangeCommand { get; private set; }
        public DelegateCommand ShiftTotalsByDateCommand { get; private set; }
        public DelegateCommand MonthlyTotalsCommand { get; private set; }
        public DelegateCommand IncomeBreakDownCommand { get; private set; }
        public DelegateCommand RoomTaxCollectedCommand { get; private set; }
        public DelegateCommand RoomTaxExemptReportCommand { get; private set; }
        public DelegateCommand RoomRevenueByDateRangeCommand { get; private set; }
        public DelegateCommand ExitCommand { get; private set; }
        public ReportsWindowViewModel(HMSService service)
        {
            _service = service;
            DailyRoomCollectedCommand = new DelegateCommand(ShowDailyRoomCollected, CanDailyRoomCollected);
            DailyAuditReportCommand = new DelegateCommand(DailyAuditReportCollected, CanDailyReportCollected);
            IncomeTaxByDateRangeCommand = new DelegateCommand(IncomeTaxByDateRange, CanIncomeTaxByDateRange);
            ShiftTotalsByDateCommand = new DelegateCommand(ShiftTotalsByDate, CanShiftTotalsByDate);
            MonthlyTotalsCommand = new DelegateCommand(MonthlyTotals, CanMonthlyTotals);
            IncomeBreakDownCommand = new DelegateCommand(IncomeBreakDown, CanIncomeBreakDown);
            RoomTaxCollectedCommand = new DelegateCommand(RoomTaxCollected, CanRoomTaxCollected);
            RoomTaxExemptReportCommand = new DelegateCommand(RoomTaxExemptReport, CanRoomTaxExemptReport);
            RoomRevenueByDateRangeCommand = new DelegateCommand(RoomRevenueByDateRange, CanRoomRevenueByDateRange);
            ExitCommand = new DelegateCommand(ExitWindow, CanExitWindow);
        }

        private bool CanRoomRevenueByDateRange(object parameter)
        {
            return true;
        }

        private bool CanExitWindow(object parameter)
        {
            return true;
        }

        private bool CanRoomTaxExemptReport(object parameter)
        {
            return true;
        }

        private bool CanRoomTaxCollected(object parameter)
        {
            return true;
        }

        private bool CanIncomeBreakDown(object parameter)
        {
            return true;
        }

        private bool CanMonthlyTotals(object parameter)
        {
            return true;
        }

        private bool CanShiftTotalsByDate(object parameter)
        {
            return true;
        }

        private bool CanIncomeTaxByDateRange(object parameter)
        {
            return true;
        }

        private bool CanDailyReportCollected(object parameter)
        {
            return true;
        }

        private void ExitWindow(object parameter)
        {
            View.Close();
        }

        private void RoomRevenueByDateRange(object parameter)
        {
            _reportGenerationViewModel = new ReportGenerationViewModel(_service, ReportEnums.EnumReportType.RoomRevenuebyDateRange, true);
            _reportGenerationView = new ReportGenerationWindow(_reportGenerationViewModel);
            _reportGenerationView.ShowDialog();
        }

        private void RoomTaxExemptReport(object parameter)
        {
            _reportGenerationViewModel = new ReportGenerationViewModel(_service, ReportEnums.EnumReportType.TaxExemptionsReport);
            _reportGenerationView = new ReportGenerationWindow(_reportGenerationViewModel);
            _reportGenerationView.ShowDialog();
        }

        private void RoomTaxCollected(object parameter)
        {
            _reportGenerationViewModel = new ReportGenerationViewModel(_service, ReportEnums.EnumReportType.IncomeByDateRange);
            _reportGenerationView = new ReportGenerationWindow(_reportGenerationViewModel);
            _reportGenerationView.ShowDialog();
        }

        private void IncomeBreakDown(object parameter)
        {
            
        }

        private void MonthlyTotals(object parameter)
        {
            _monthlyReportsViewModel = new MonthlyReportsViewModel(_service);
            _monthlyReportsView = new MonthlyReports(_monthlyReportsViewModel);
            _monthlyReportsView.ShowDialog();
        }

        private void ShiftTotalsByDate(object parameter)
        {
            
        }

        private void IncomeTaxByDateRange(object parameter)
        {
            _reportGenerationViewModel = new ReportGenerationViewModel(_service, ReportEnums.EnumReportType.IncomeByDateRange);
            _reportGenerationView = new ReportGenerationWindow(_reportGenerationViewModel);
            _reportGenerationView.ShowDialog();
        }

        private void DailyAuditReportCollected(object parameter)
        {
            _reportGenerationViewModel = new ReportGenerationViewModel(_service, ReportEnums.EnumReportType.DailyAuditReport);
            _reportGenerationView = new ReportGenerationWindow(_reportGenerationViewModel);
            _reportGenerationView.ShowDialog();
        }

        private void ShowDailyRoomCollected(object parameter)
        {
            _reportGenerationViewModel = new ReportGenerationViewModel(_service,ReportEnums.EnumReportType.DailyRoomAndTax);
            _reportGenerationView = new ReportGenerationWindow(_reportGenerationViewModel);
            _reportGenerationView.ShowDialog();
        }

        private bool CanDailyRoomCollected(object parameter)
        {
            return true;
        }
    }
}
