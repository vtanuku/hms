﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Model;
using HMS.Utils;
using HMS.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HMS.ViewModel
{
    public class RoomsLayoutViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<RoomsOccupancyViewModel> _floorsViewModel;
        private HMSService _hmsService;
        private int _dirtyCount;
        private int _rentedCount;
        private int _vacantCount;
        private int _repairCount;
        private int _allRoomCount;
        private ObservableCollection<RoomsOccupancyViewModel> _floorsTempViewModel;
        private Visibility _setPBVisibility;

        public DelegateCommand InitializeViewModelCommand { get; set; }
        public DelegateCommand OpenHotelRoomDetailsCommand { get; set; }
        public GuestWindow View { get; set; }
        public ObservableCollection<RoomsOccupancyViewModel> FloorsViewModels
        {
            get
            {
                return _floorsViewModel;
            }
            set
            {
                _floorsViewModel = value;
                OnPropertyChanged("FloorsViewModels");
            }
        }

        public int DirtyCount
        {
            get
            {
                return _dirtyCount;
            }
            set
            {
                _dirtyCount = value;
                OnPropertyChanged();
            }
        }
        public int RepairCount
        {
            get
            {
                return _repairCount;
            }
            set
            {
                _repairCount = value;
                OnPropertyChanged();
            }
        }
        public int VacantCount
        {
            get
            {
                return _vacantCount;
            }
            set
            {
                _vacantCount = value;
                OnPropertyChanged();
            }
        }
        public int RentedCount
        {
            get
            {
                return _rentedCount;
            }
            set
            {
                _rentedCount = value;
                OnPropertyChanged();
            }
        }

        public int AllRoomCount
        {
            get
            {
                return _allRoomCount;
            }
            set
            {
                _allRoomCount = value;
                OnPropertyChanged();
            }
        }

        public Visibility SetPBVisibility
        {
            get
            {
                return _setPBVisibility;
            }
            set
            {
                _setPBVisibility = value;
                OnPropertyChanged();
            }
        }

        public RoomsLayoutViewModel(HMSService hmsService)
        {
            _hmsService = hmsService;
            InitializeViewModelCommand = new DelegateCommand(SetRoomsLayout, CanSet);
            OpenHotelRoomDetailsCommand = new DelegateCommand(ProcessWindowCommand, CanProcessWindow);
            _floorsViewModel = new ObservableCollection<RoomsOccupancyViewModel>();
            _floorsTempViewModel = new ObservableCollection<RoomsOccupancyViewModel>();
            SetPBVisibility = Visibility.Collapsed;
            FloorsViewModels.CollectionChanged += OnCollectionChanged;
        }

        private void OnCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }

        private void ProcessWindowCommand(object parameter)
        {
            var room = _hmsService.GetRoomDetails(parameter.ToString());
            if (room.StatusId == 1)
            {
                var arrivals = _hmsService.GetArrivals(room.Room_Display);
                if (arrivals != null)
                {
                    var guest = _hmsService.GetGuests(arrivals.Guest.Id);
                    var viewModel = new GuestWindowViewModel(guest, arrivals, Utils.RoomStatusEnum.Vacant, _hmsService);
                    View = new GuestWindow(viewModel);
                    View.ShowDialog();
                    if (View.DialogResult == true)
                    {
                        _hmsService.UpdateStatusOfRoom(room.Id, 3);
                        room.StatusId = 3;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).StatusId = 3;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).GuestName = string.Empty;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).TentativeCheckOutDate = null;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).CheckInDate = null;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).NumberofHoursLeft = 0;
                    }
                }
            }
            else if (room.StatusId == 2)
            {
                var result = MessageBox.Show("Do you want to Check in a guest or do you want to Mark the room as dirty?", "What do you want to do", MessageBoxButton.YesNo, MessageBoxImage.None);
                if (result == MessageBoxResult.Yes)
                {
                    var guest = new Model.Guests();
                    var arrival = new Model.Arrivals(guest, room);
                    var viewModel = new GuestWindowViewModel(guest, arrival, RoomStatusEnum.CheckIn, _hmsService);
                    View = new GuestWindow(viewModel);
                    View.ShowDialog();
                    if (View.DialogResult == true)
                    {
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).StatusId = 1;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).GuestName = guest.FullName;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).TentativeCheckOutDate = arrival.TentativeCheckoutDate;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).CheckInDate = arrival.CheckInDate;
                        _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).NumberofHoursLeft = (arrival.CheckInDate - arrival.TentativeCheckoutDate).Hours;
                    }
                }
                else if (result == MessageBoxResult.No)
                {
                    _hmsService.ShiftToMaintenance(room);
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).StatusId = 3;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).GuestName = string.Empty;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).TentativeCheckOutDate = null;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).CheckInDate = null;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).NumberofHoursLeft = 0;

                }
            }
            else if (room.StatusId == 3)
            {
                var result = MessageBox.Show("Do you want to mark the Room from Repair to Normal?", "Choose One", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    _hmsService.ShiftToNormal(room);
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).StatusId = 2;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).GuestName = string.Empty;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).TentativeCheckOutDate = null;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).NumberofHoursLeft = 0;
                }
            }
            else if (room.StatusId == 3 || room.StatusId == 4)
            {
                var result = MessageBox.Show("Do you want to mark the Room from Repair to Normal?", "Choose One", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    _hmsService.ShiftToNormal(room);
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).StatusId = 2;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).GuestName = string.Empty;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).TentativeCheckOutDate = null;
                    _floorsViewModel.FirstOrDefault(f => f.RoomName == room.Room_Display).NumberofHoursLeft = 0;
                }
            }
        }

        private bool CanProcessWindow(object parameter)
        {
            return true;
        }

        private bool CanSet(object parameter)
        {
            return true;
        }

        private async void SetRoomsLayout(object parameter)
        {
            var rooms = await Task.Run(() => _hmsService.GetRooms().ToList());
            _floorsViewModel?.Clear();
            DirtyCount = rooms.Count(c => c.StatusId == 4);
            RepairCount = rooms.Count(c => c.StatusId == 3);
            VacantCount = rooms.Count(c => c.StatusId == 2);
            RentedCount = rooms.Count(c => c.StatusId == 1);
            AllRoomCount = rooms.Count;
            await ConstructTheViewModels(rooms);
        }

        private async Task ConstructTheViewModels(IEnumerable<Rooms> rooms)
        {
            SetPBVisibility = Visibility.Visible;
            foreach (var item in rooms)
            {
                RoomsOccupancyViewModel _viewModel = new RoomsOccupancyViewModel();
                var arrival = await Task.Run(() => _hmsService.GetArrivals(item.Room_Display));
                _viewModel.RoomName = item.Room_Display;
                _viewModel.RoomType = _hmsService.GetRoomTypes().FirstOrDefault(t => t.Id == item.RoomType?.Id).Code;
                if (arrival != null && arrival.CheckInDate != DateTime.MinValue && arrival.Guest != null)
                {
                    var guest = await Task.Run(() => _hmsService.GetGuests(arrival.Guest.Id));
                    _viewModel.GuestName = guest?.FullName;
                    _viewModel.CheckInDate = arrival.CheckInDate == DateTime.MinValue ? (DateTime?)null : arrival.CheckInDate;
                    _viewModel.TentativeCheckOutDate = arrival.TentativeCheckoutDate;
                    _viewModel.NumberofHoursLeft = (arrival.TentativeCheckoutDate - DateTime.Now).Hours;
                }
                _viewModel.StatusId = item.StatusId;
                _floorsTempViewModel.Add(_viewModel);
                
            }
            SetPBVisibility = Visibility.Hidden;
            FloorsViewModels = _floorsTempViewModel;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
