﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Model;
using HMS.Utils;
using HMS.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HMS.ViewModel
{
    public class RoomViewModel : INotifyPropertyChanged
    {
        private HMSService _hmsService;
        private string _roomNumber;
        private GuestWindow _guestView;
        private string _buttonText;
        private Arrivals _arrivals;
        private Guests _guests;

        public RoomViewModel(HMSService hmsService, Guests guests, Arrivals arrivals)
        {
            _hmsService = hmsService;
            _guests = guests;
            _arrivals = arrivals;
            CheckoutDetailsCommand = new DelegateCommand(CheckoutRoom, CanCheckOut);
        }

        private bool CanCheckOut(object parameter)
        {
            return true;
        }

        private void CheckoutRoom(object parameter)
        {
            if (ButtonText == "Transfer")
            {
                _hmsService.CheckOutGuest(_guests, _arrivals);
                var roomID = _hmsService.GetRooms().FirstOrDefault(f => f.RoomNumber == RoomNumber);
                _arrivals.OldRoom = _arrivals.RoomId?.Id.ToString();
                _arrivals.RoomId = roomID;
                _hmsService.CheckInGuest(_guests, _arrivals);                
            }
            else
            {
                var arrival = _hmsService.GetArrivals(RoomNumber.ToString());
                var guest = _hmsService.GetGuests(arrival.Guest.Id);
                var viewModel = new GuestWindowViewModel(guest, arrival, RoomStatusEnum.Vacant, _hmsService);
                _guestView = new GuestWindow(viewModel);
                _guestView.ShowDialog();
            }
        }

        public DelegateCommand CheckoutDetailsCommand { get; private set; }

        public string RoomNumber
        {
            get
            {
                return _roomNumber;
            }
            set
            {
                _roomNumber = value;
                OnPropertyChanged();
            }
        }

        public string ButtonText
        {
            get
            {
                return _buttonText;
            }
            set
            {
                _buttonText = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
