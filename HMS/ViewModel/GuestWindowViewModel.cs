﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Model;
using HMS.Model.Lookups;
using HMS.Utils;
using HMS.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HMS.ViewModel
{
    public class GuestWindowViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public DelegateCommand SubmitFormCommand { get; set; }
        public GuestWindowViewModel(Guests guest, Arrivals arrival, RoomStatusEnum roomStatus, HMSService hmsService)
        {
            _guest = guest;
            _arrival = arrival;
            _hmsService = hmsService;
            FormButtonText = roomStatus.ToString();
            CheckinTypes = new ObservableCollection<string>();
            RoomCharge = (_arrival.RoomCharge == null ? _arrival.RoomId?.RoomType?.RoomCharge + _arrival.RoomId?.RoomType?.RoomServiceTax : _arrival.RoomCharge + _arrival.RoomServiceTax);
            IsCheckinTypeReadOnly = false;
            ServiceTax = _arrival.RoomId?.RoomType?.RoomServiceTax;
            SubmitFormCommand = new DelegateCommand(SubmitForm, CanSubmitForm);
            PrintFormCommand = new DelegateCommand(PrintBill, CanPrintForm);
            ExtendTimeCommand = new DelegateCommand(ExtendTime, CanExtendTime);
            _roomTypes = new ObservableCollection<RoomType>();
            _hmsService.GetRoomTypes().ForEach(rt=>_roomTypes.Add(rt));
            TransferRoomCommand = new DelegateCommand(TransferRoom, CanTransferRoom);
            CheckinTypes.Add("Daily");
            CheckinTypes.Add("Weekly");
            CheckinTypes.Add("Monthly");
            if (FormButtonText == "Checkin")
            {
                _arrival.CheckInDate = _arrival.CheckInDate == DateTime.MinValue ? DateTime.Now : _arrival.CheckInDate;
                ExtendButtonVisible = Visibility.Collapsed;
            }
            if(FormButtonText == "Vacant")
            {
                CheckoutEnabled = true;
                ExtendButtonVisible = Visibility.Visible;
            }
            else
            {
                CheckoutEnabled = false;
            }            

            SelectedRoomType = _roomTypes.FirstOrDefault(f => f.Id == _arrival.RoomId?.RoomType?.Id);
        }

        private void ExtendTime(object parameter)
        {
            _hmsService.ExtendTime(_arrival);
        }

        private bool CanExtendTime(object parameter)
        {
            return true;
        }

        private void PrintBill(object parameter)
        {
            _hmsService.UpdateFinalDetails(_arrival, RoomCharge, ServiceTax, CentralGST, StateGST);
        }

        private bool CanPrintForm(object parameter)
        {
            return true;
        }

        private bool CanTransferRoom(object parameter)
        {
            return true;
        }

        private void TransferRoom(object parameter)
        {
            if(MessageBox.Show("Do you want to really transfer the Guest to a new room?", "Warning", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                var viewModel = new RoomViewModel(_hmsService, _guest, _arrival);
                _roomView = new RoomView(viewModel, true);
                _roomView.ShowDialog();
            }
        }

        private void SubmitForm(object parameter)
        {
            if(parameter.ToString() == RoomStatusEnum.CheckIn.ToString())
            {
                _hmsService.CheckInGuest(_guest,_arrival, CheckoutEnabled);
            }
            else if (parameter.ToString() == RoomStatusEnum.Vacant.ToString())
            {
                _hmsService.CheckOutGuest(_guest, _arrival);
            }
            else if(parameter.ToString() == RoomStatusEnum.UnderMaintenance.ToString())
            {
                _hmsService.ShiftToMaintenance(_room);
            }
            else if(parameter.ToString() == RoomStatusEnum.Dirty.ToString())
            {
                _hmsService.ShiftToDirty(_room);
            }
            else if (parameter.ToString() == RoomStatusEnum.Transfer.ToString())
            {
                _hmsService.TransferRoom(_guest, _arrival);
            }
        }

        private bool CanSubmitForm(object parameter)
        {
            return true;
        }

        public int GuestID
        {
            get
            {
                return _guest.Id;
            }
            set
            {
                _guest.Id = value;
                OnPropertyChanged();
            }
        }

        public string FormButtonText
        {
            get
            {
                return _formButtonText;
            }
            set
            {
                _formButtonText = value;
                OnPropertyChanged();
            }
        }

        private Guests _guest;
        private Arrivals _arrival;
        private string _guestRemarks;
        private List<PaymentModes> _paymentModes;
        private List<Proofs> _listOfProofs;
        private string _cvv;
        private string _formButtonText;
        private HMSService _hmsService;
        private Rooms _room;
        private bool _checkoutEnabled;
        private RoomView _roomView;
        private ObservableCollection<RoomType> _roomTypes;
        private decimal? _roomCharge;
        private ObservableCollection<string> _checkinTypes;
        
        private bool _isCheckinTypeReadOnly;
        private Visibility _extendButtonVisible;

        public string GuestName
        {
            get
            {
                return _guest.FullName;
            }
            set
            {
                _guest.FullName = value;
                OnPropertyChanged();
            }
        }

        public DateTime DateofBirth
        {
            get
            {
                return _guest.DateOfBirth == DateTime.MinValue ? DateTime.Now : _guest.DateOfBirth;
            }
            set
            {
                _guest.DateOfBirth = value;
                OnPropertyChanged();
            }
        }

        public string Gender
        {
            get
            {
                return _guest.Gender;
            }
            set
            {
                _guest.Gender = value;
                OnPropertyChanged();
            }
        }
        
        public bool Male
        {
            get
            {
                return _guest.Gender == "M" ? true : false;
            }
            set
            {
                _guest.Gender = value ? "M" : "F";
                OnPropertyChanged();
            }
            
        }

        public bool ThroughCash
        {
            get
            {
                return _arrival.ModeofPaymentId?.Id == 1 ? true : false;
            }
            set
            {
                if (value)
                {
                    _arrival.ModeofPaymentId = _hmsService.GetPaymentModes()?.FirstOrDefault(m => m.Id == 1);
                }
            }
        }

        public bool ThroughCard
        {
            get
            {
                return _arrival.ModeofPaymentId?.Id == 2 ? true : false;
            }
            set
            {
                if(value)
                {
                    _arrival.ModeofPaymentId = _hmsService.GetPaymentModes()?.FirstOrDefault(m => m.Id == 2);
                }
            }
        }

        public bool Female
        {
            get
            {
                return _guest.Gender == "F" ? true : false;
            }
            set
            {
                _guest.Gender = value ? "F" : "M";
            }
        }

        public string Address
        {
            get
            {
                return _guest.Address;
            }
            set
            {
                _guest.Address = value;
                OnPropertyChanged();
            }
        }

        public string IDProof
        {
            get
            {
                return _guest.IdProofType;
            }
            set
            {
                _guest.IdProofType =  value;
                OnPropertyChanged();
            }
        }

        public string IDNumber
        {
            get
            {
                return _guest.IdProofNumber;
            }
            set
            {
                _guest.IdProofNumber = value;
                OnPropertyChanged();
            }
        }

        public string VehicleModel
        {
            get
            {
                return _guest.CarModel;
            }
            set
            {
                _guest.CarModel = value;
                OnPropertyChanged();
            }
        }

        public string VehicleNumber
        {
            get
            {
                return _guest.RTGNumber;
            }
            set
            {
                _guest.RTGNumber = value;
                OnPropertyChanged();
            }
        }

        public DateTime CheckinDate
        {
            get
            {
                _arrival.CheckInDate =  _arrival.CheckInDate == DateTime.MinValue ? DateTime.Now : _arrival.CheckInDate;
                return _arrival.CheckInDate;
            }
            set
            {
                _arrival.CheckInDate = value;
                OnPropertyChanged();
            }
        }

        public string CheckinTime
        {
            get
            {
                return _arrival.CheckInDate.ToString("HH:MM");
            }           
            set
            {
                TimeSpan time;
                value = value.Replace("Time:", string.Empty);
                if (TimeSpan.TryParse(value, out time))
                {
                    _arrival.CheckInDate = _arrival.CheckInDate.AddHours(time.Hours);
                    _arrival.CheckInDate = _arrival.CheckInDate.AddMinutes(time.Minutes);
                }
                OnPropertyChanged();
            } 
        }

        public DateTime? CheckoutDate
        {
            get
            {
                return _arrival.CheckoutDate == DateTime.MinValue ? (DateTime?)null : _arrival.CheckoutDate;
            }
            set
            {
                _arrival.CheckoutDate = value;
                OnPropertyChanged();
            }
        }

        public DateTime TentativeCheckoutDate
        {
            get
            {
                return _arrival.TentativeCheckoutDate == DateTime.MinValue ? DateTime.Now : _arrival.TentativeCheckoutDate;
            }
            set
            {
                _arrival.TentativeCheckoutDate = value;
                OnPropertyChanged();
            }
        }

        public int Daily
        {
            get
            {
                return _arrival.NumberofDays;
            }
            set
            {
                _arrival.NumberofDays = value;
                RoomCharge = _arrival.RoomCharge * value;

                OnPropertyChanged();
            }
        }

        public int NumberOfGuests
        {
            get
            {
                return _arrival.NumberofGuests;
            }
            set
            {
                _arrival.NumberofGuests = value;
                OnPropertyChanged();
            }
        }

        public string TentativeCheckoutTime
        {
            get
            {
                return _arrival.TentativeCheckoutDate.ToString("HH:MM");
            }
            set
            {
                TimeSpan time;
                value = value.Replace("Time:", string.Empty);
                if (TimeSpan.TryParse(value, out time))
                {
                    _arrival.TentativeCheckoutDate = _arrival.TentativeCheckoutDate.AddHours(time.Hours);
                    _arrival.TentativeCheckoutDate = _arrival.TentativeCheckoutDate.AddMinutes(time.Minutes);
                }
                OnPropertyChanged();
            }
        }

        public string CheckoutTime
        {
            get
            {
                return _arrival.CheckoutDate?.ToString("HH:MM");
            }
            set
            {
                TimeSpan time;
                value = value.Replace("Time:", string.Empty);
                if (TimeSpan.TryParse(value, out time))
                {
                    _arrival.CheckoutDate=_arrival.CheckoutDate?.AddHours(time.Hours);
                    _arrival.CheckoutDate = _arrival.CheckoutDate?.AddMinutes(time.Minutes);
                }
                OnPropertyChanged();
            }
        }

        public PaymentModes ModeOfPayment
        {
            get
            {
                return _arrival.ModeofPaymentId;
            }
            set
            {
                _arrival.ModeofPaymentId = value;
                OnPropertyChanged();
            }
        }

        public RoomType SelectedRoomType
        {
            get {
                    return _roomTypes.FirstOrDefault(f=>f.Id ==  _arrival.RoomType);
                }
            set {
                    _arrival.RoomType = value == null ? 0 :  value.Id; OnPropertyChanged();
                    _arrival.RoomCharge = _arrival.RoomCharge == null || _arrival.RoomCharge == 0 ?  value?.RoomCharge : _arrival.RoomCharge;
                    _arrival.RoomServiceTax = _arrival.RoomServiceTax == null || _arrival.RoomServiceTax == 0 ? value?.RoomServiceTax : _arrival.RoomServiceTax;
                    _arrival.CentralGST = _arrival.CentralGST == null || _arrival.CentralGST == 0 ? value?.CentralGST : _arrival.CentralGST;
                    _arrival.StateGST = _arrival.StateGST == null || _arrival.StateGST == 0 ? value?.StateGST : _arrival.StateGST;
            }
        }

        public decimal? RoomRate
        {
            get
            {
                return _arrival.RoomRate ?? 0;
            }
            set
            {
                _arrival.RoomRate = value;
                OnPropertyChanged();
                CalculateBalance();
            }
        }

        private void CalculateBalance()
        {
            Balance = RoomRate - (RoomCharge + CentralGST + StateGST + Deposit - Misc);
        }

        public string RoomNumber
        {
            get {return _arrival.RoomId?.RoomNumber?.ToString(); }
            set
            {
                 if(_arrival.RoomId == null)
                {
                    _arrival.RoomId = new Rooms();                    
                }
                _arrival.RoomId.RoomNumber = value;
                SelectedRoomType = _roomTypes.FirstOrDefault(f=>f.Id == _arrival.RoomId.RoomType?.Id);
                OnPropertyChanged();
            }
        }

        public decimal? RoomCharge
        {
            get
            {
                return _roomCharge;
            }
            set
            {
                _roomCharge = value;
                _arrival.RoomCharge = value;
                OnPropertyChanged();
                CalculateBalance();
            }
        }

        public decimal? ServiceTax
        {
            get
            {
                return _arrival.RoomServiceTax;
            }
            set
            {
                _arrival.RoomServiceTax=value;
            }
        }

        public decimal? Misc
        {
            get
            {
                return _arrival.Misc ?? 0;
            }
            set
            {
                _arrival.Misc = value;
                OnPropertyChanged();
                CalculateBalance();
            }
        }

        public decimal? RoomTax
        {
            get
            {
                return _arrival.Tax;
            }
            set
            {
                _arrival.Tax = value;
                OnPropertyChanged();
            }
        }

        public decimal? RoomTaxExempt
        {
            get
            {
                return _arrival.TaxExemption;
            }
            set
            {
                _arrival.TaxExemption = value;
                OnPropertyChanged();
            }
        }

        public decimal? Cash { get { return _arrival.Cash ?? 0; } set { _arrival.Cash = value; OnPropertyChanged(); }}
        public decimal? StateGST { get { return _arrival.StateGST ?? 0; } set { _arrival.StateGST = value; OnPropertyChanged(); CalculateBalance(); } }
        public decimal? CentralGST { get { return _arrival.CentralGST ?? 0; } set { _arrival.CentralGST = value; OnPropertyChanged(); CalculateBalance(); } }
        public decimal? Deposit { get { return _arrival.Deposit ?? 0; } set { _arrival.Deposit = value; OnPropertyChanged(); CalculateBalance(); }}
        public decimal? Balance { get { return _arrival.Balance ?? 0; } set { _arrival.Balance = value; OnPropertyChanged(); }}
        public decimal? Tax { get { return _arrival.Tax ?? 0; } set { _arrival.Tax = value; OnPropertyChanged(); }}
        
        public string ZipCode { get {return _guest.Zipcode;} set { _guest.Zipcode = value;OnPropertyChanged(); }}
        public string City { get {return _guest.City;} set { _guest.City = value;OnPropertyChanged(); }}
        public string State { get {return _guest.State;} set { _guest.State = value;OnPropertyChanged(); }}
        public string PhoneNumber { get {return _guest.Phonenumber;} set { _guest.Phonenumber = value;OnPropertyChanged(); }}
        public string Emailid { get {return _guest.Emailid;} set { _guest.Emailid = value;OnPropertyChanged(); }}
        public string MoreIds { get {return _guest.MoreIds;} set { _guest.MoreIds = value;OnPropertyChanged(); }}
        public string PurposeOfVisit { get {return _guest.Purposeofvisit;} set { _guest.Purposeofvisit = value;OnPropertyChanged(); }}
        public string CompanyName { get {return _guest.Company;} set { _guest.Company = value;OnPropertyChanged(); }}
        public string CardNumber
        {
            get
            {
                return _arrival.CardNumber;
            } 
            set
            {
                _arrival.CardNumber = value;
            }
        }

        public DateTime ExpiryDate
        {
            get
            {
                return _arrival.ExpiryDate == DateTime.MinValue ? DateTime.Now : _arrival.ExpiryDate;
            }
            set
            {
                _arrival.ExpiryDate = value;
            }
        }

        public string CVV
        {
            get
            {
                return _cvv;
            }
            set
            {
                _cvv = value;
            }
        }

        public string GuestRemarks
        {
            get
            {
                return _arrival.GuestRemarks;
            }
            set
            {
                _arrival.GuestRemarks = value;
                OnPropertyChanged();
            }
        }

        public List<PaymentModes> PayMentModes
        {
            get
            {
                return _paymentModes;
            }
            set
            {
                _paymentModes = value;
                OnPropertyChanged();
            }
        }

        public List<Proofs> ListOfProofs
        {
            get
            {
                return _listOfProofs;
            }

            set
            {
                _listOfProofs = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand TransferRoomCommand { get; private set; }
        public bool CheckoutEnabled
        {
            get
            {
                return _checkoutEnabled;
            }
            set
            {
                _checkoutEnabled = value;
                OnPropertyChanged();
            }
        }

        public string Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public ObservableCollection<RoomType> RoomTypes
        {
            get
            {
                return _roomTypes;
            }
            set
            {
                _roomTypes = value;
                OnPropertyChanged();
            }
        }

        public bool IsCheckinTypeReadOnly
        {
            get
            {
                return _isCheckinTypeReadOnly;
            }
            set
            {
                _isCheckinTypeReadOnly = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<string> CheckinTypes
        {
            get
            {
                return _checkinTypes;
            }
            set
            {
                _checkinTypes = value;
                OnPropertyChanged();
            }
        }

        public string SelectedCheckinType
        {
            get
            {
                return _arrival.CheckinType;
            }
            set
            {
                _arrival.CheckinType = value;
                if(value == "Weekly")
                {
                    Daily = 7;
                }
                else if(value == "Monthly")
                {
                    Daily = 30;
                }
                OnPropertyChanged();
            }
        }

        public Visibility ExtendButtonVisible
        {
            get
            {
                return _extendButtonVisible;
            }
            set
            {
                _extendButtonVisible = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand PrintFormCommand { get; set; }
        public DelegateCommand ExtendTimeCommand { get; set; }

        public string this[string columnName]
        {
            get
            {
                string result = null;
                if(columnName == "CheckinDate")
                {
                    if(FormButtonText == "CheckIn")
                    {
                        if(CheckinDate==DateTime.MinValue)
                        {
                            result = "Check in date is mandatory";
                        }
                    }
                }
                if (columnName == "RoomNumber")
                {
                    if (FormButtonText == "CheckIn")
                    {
                        var room = _hmsService.GetRoomDetails(_arrival.RoomId?.RoomNumber);
                        if (room == null)
                        {
                            result = "Invalid RoomNumber";
                        }
                        if (room?.StatusId == 1)
                        {
                            result = "Room Already Occupied. Please verify";
                        }
                    }
                }
                return result;
            }
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
