﻿using System;
using System.Windows;
using HMS.Core.Command;
using HMS.DAL.Reposotories;
using HMS.Views;
using HMS.Views.Sub_Menus;
using HMS.Utils;
using System.Collections.Generic;
using HMS.Model;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using HMS.Core.Utils;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using HMS.DAL.DAL.Model;

namespace HMS.ViewModel
{
    public class HotelManagSystemViewModel : INotifyPropertyChanged
    {
        private RoomsLayoutViewModel _roomsLayoutViewModel;

        public RoomsLayout View { get; private set; }
        public RelayCommand<Window> CloseWindowCommand { get; set; }

        private User _user;

        public Action RequestClose { get; internal set; }
        public DelegateCommand ShowRoomsCommand { get; set; }
        public DelegateCommand ShowManagersMenuCommand { get; private set; }
        public DelegateCommand ShowCheckInsCommand { get; private set; }
        public DelegateCommand ShowCheckOutsCommand { get; private set; }
        public DelegateCommand ShowReportsCommand { get; private set; }
        public DelegateCommand ShowRoomMaintenanceCommand { get; set; }
        public DelegateCommand ShowAddUserCommand { get; private set; }

        private HMSService _service;
        private ManagersWindowViewModel _managersWindowViewModel;
        private ManagersMenu managersView;
        private ReportsWindowViewModel _reportsWindowViewModel;
        private ReportsWindow _reportsWindow;
        private GuestWindow _guestView;
        private RoomMaintenanceViewModel _roomMaintenanceViewModel;
        private RoomMaintenance _roomMaintenanceView;
        private RoomView _roomView;
        private Timer timer1;
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<GraphSeriesDataPoint> Warnings
        {
            get;set;
        }
        public HotelManagSystemViewModel(HMSService service, User user)
        {
            CloseWindowCommand = new RelayCommand<Window>(CloseWindow);
            _user = user;
            ShowRoomsCommand = new DelegateCommand(ShowRoomsLayout, CanShowRooms);
            ShowManagersMenuCommand = new DelegateCommand(ShowManagersMenu, CanShowManagersMenu);
            ShowCheckInsCommand = new DelegateCommand(ShowCheckIns, CanShowMCheckIns);
            ShowCheckOutsCommand = new DelegateCommand(ShowCheckOuts, CanSShowCheckOuts);
            ShowReportsCommand = new DelegateCommand(ShowReports, CanShowReports);
            Warnings = new ObservableCollection<GraphSeriesDataPoint>();
            ShowRoomMaintenanceCommand = new DelegateCommand(ShowRoomMaintenanceWindow, CanShowRoomMaintenance);
            _service = service;
        }

        public void PopulateGraph()
        {
                var rooms = _service.GetRooms();
                Warnings.Clear();
                Warnings.Add(new GraphSeriesDataPoint("Rented", rooms.Count(r => r.StatusId == 1)));
                Warnings.Add(new GraphSeriesDataPoint("Vacant", rooms.Count(r => r.StatusId == 2)));
                Warnings.Add(new GraphSeriesDataPoint("Repair", rooms.Count(r => r.StatusId == 3)));
                Warnings.Add(new GraphSeriesDataPoint("Dirty", rooms.Count(r => r.StatusId == 4)));
            
        }

        private void ShowRoomMaintenanceWindow(object parameter)
        {
            
            _roomMaintenanceViewModel = new RoomMaintenanceViewModel(_service);
            _roomMaintenanceView = new RoomMaintenance(_roomMaintenanceViewModel);
            _roomMaintenanceView.ShowDialog();
        }

        private bool CanShowRoomMaintenance(object parameter)
        {
            return true;
        }

        private void ShowReports(object parameter)
        {
            _reportsWindowViewModel = new ReportsWindowViewModel(_service);
            _reportsWindow = new ReportsWindow(_reportsWindowViewModel);
            _reportsWindow.ShowDialog();
        }

        private void ShowCheckOuts(object parameter)
        {
            var viewModel = new RoomViewModel(_service, null,null);
            _roomView = new RoomView(viewModel);
            _roomView.ShowDialog();
        }

        private void ShowCheckIns(object parameter)
        {
            var guest = new Model.Guests();
            var room = new Model.Rooms();
            var viewModel = new GuestWindowViewModel(guest, new Model.Arrivals(guest, room), RoomStatusEnum.CheckIn, _service);
            _guestView = new GuestWindow(viewModel);
            _guestView.ShowDialog();
        }

        private void ShowManagersMenu(object parameter)
        {
            if (_user != null && _user.RoleId == 1)
            {
                _managersWindowViewModel = new ManagersWindowViewModel(_service);
                managersView = new ManagersMenu(_managersWindowViewModel);
                managersView.ShowDialog();
            }
            else
            {
                MessageBox.Show("No permission to this menu");
            }
        }

        private bool CanShowManagersMenu(object parameter)
        {
            return true;
        }

        private bool CanShowMCheckIns(object parameter)
        {
            return true;
        }

        private bool CanSShowCheckOuts(object parameter)
        {
            return true;
        }

        private bool CanShowReports(object parameter)
        {
            return true;
        }

        private void ShowRoomsLayout(object parameter)
        {
            try
            {
                _roomsLayoutViewModel = new RoomsLayoutViewModel(_service);
                View = new RoomsLayout(_roomsLayoutViewModel);
                View.ShowDialog();
            }
            catch (Exception ex)
            {

            }
        }

        private bool CanShowRooms(object parameter)
        {
            return true;
        }

        private void CloseWindow(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
