﻿using HMS.Core.Command;
using HMS.DAL.Reposotories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HMS.ViewModel
{
    public class RoomTypesWindowViewModel :INotifyPropertyChanged
    {
        private HMSService _hmsService;
        private RoomTypeDetailViewModel _selectedRoomType;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<RoomTypeDetailViewModel> RoomTypesDetails { get; set; }
        public RoomTypesWindowViewModel(HMSService hmsService)
        {
            _hmsService = hmsService;
            RoomTypesDetails = new ObservableCollection<RoomTypeDetailViewModel>();
            AddRoomTypeCommand = new DelegateCommand(AddRoomType, CanAddRoomType);
            DeleteRoomTypeCommand = new DelegateCommand(DeleteRoomType, CanDeleteRoomType);
            SaveCommand = new DelegateCommand(Save, CanSave);
            _hmsService.GetRoomTypes().ForEach(f => RoomTypesDetails.Add(new RoomTypeDetailViewModel(f)));
        }

        private bool CanSave(object parameter)
        {
            return true;
        }

        private void Save(object parameter)
        {
            foreach (var item in RoomTypesDetails.Where(r=>r.IsDirty))
            {
                if(item.Id == 0)
                {
                    _hmsService.InsertRoomType(item.RoomType);
                }
                else
                {
                    _hmsService.UpdateRoomType(item.RoomType);
                }
            }
            MessageBox.Show("Room types updated Successfully");
        }

        private bool CanDeleteRoomType(object parameter)
        {
            return true;
        }

        private void AddRoomType(object parameter)
        {
            var roomType = new RoomTypeDetailViewModel(new Model.Lookups.RoomType());
            RoomTypesDetails.Add(roomType);            
        }
        private void DeleteRoomType(object parameter)
        {
            if(_hmsService.GetRooms().Any(r=>r.RoomType?.Id == SelectedRoomType.Id))
            {
                MessageBox.Show("Cannot Delete the room type as there are rooms already associated with it");
            }
            else
                RoomTypesDetails.Remove(SelectedRoomType);
        }

        private bool CanAddRoomType(object parameter)
        {
            return true;
        }

        public RoomTypeDetailViewModel SelectedRoomType
        {
            get
            {
                return _selectedRoomType;
            }
            set
            {
                _selectedRoomType = value;
                OnPropertyChanged();
            }
        }

        public DelegateCommand AddRoomTypeCommand { get; private set; }
        public DelegateCommand DeleteRoomTypeCommand { get; private set; }
        public DelegateCommand SaveCommand { get; private set; }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
