﻿using System;
using System.Windows;

namespace CollageImageControl
{
    public class Photo
    {
        public Point Location;
        public String FileName;

        public Photo()
        {

        }

        public Photo(Point location, String filename)
        {
            this.Location.X = location.X;
            this.Location.Y = location.Y;
            this.FileName = filename;
        }
    }
}