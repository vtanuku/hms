﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.Enums
{
    public class ReportEnums
    {
        public enum EnumReportType
        {
            [Description(@"Daily Room and Tax Collection")]
            DailyRoomAndTax = 1,
            [Description(@"Daily Audit")]
            DailyAuditReport,
            [Description(@"Income By Date Range")]
            IncomeByDateRange,
            [Description(@"Shift Totals By Date Range")]
            ShiftTotalsByDateRange,
            [Description(@"Income Break Down")]
            IncomeBreakDown,
            [Description(@"Room Tax Date Range")]
            RoomTaxDateRange,
            [Description(@"Tax Exemptions Report")]
            TaxExemptionsReport,
            [Description(@"Room Revenue by Date Range Report")]
            RoomRevenuebyDateRange,
            [Description(@"Monthly Totals")]
            MonthlyTotals
        }
    }
}
