﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HMS.Core.Utils
{
    public class GraphSeriesDataPoint : INotifyPropertyChanged
    {
        private string _date;
        public string date
        {
            get
            {
                return _date;
            }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    OnPropertyChanged("date");
                }
            }
        }

        private double _amount;

        public event PropertyChangedEventHandler PropertyChanged;

        public double amount
        {
            get
            {
                return _amount;
            }
            set
            {
                if (_amount != value)
                {
                    _amount = value;
                    OnPropertyChanged("amount");
                }
            }
        }

        public GraphSeriesDataPoint(string date, double amount)
        {
            this.date = date;
            this.amount = amount;
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

